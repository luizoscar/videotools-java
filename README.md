# Video Files conversion tool

This is a Java application to simplify the process of converting and making smal changes to video files using the [open source media conversion tool ffmpeg](https://www.ffmpeg.org/).  
Starting from a given directory, the application will search and display the list of video files with the respective codec media informations.  
On the right side, there is a toolbar with the actions that can be applied to the selected files.  
This application requires the ffmpeg binary and Java 8 os supperior, it can be executed on Linux and Windows.  

**Important:**

In order to support some features like H265 HEVC, AOMedia AV1 Codec and video stabilization, sometimes it is necessary to recompile ffmpeg to enable such functionality; This can be done following the [Official guide to compile FFmpeg for Ubuntu, Debian, or Mint](https://trac.ffmpeg.org/wiki/CompilationGuide).

## Supported formats

The application is able to convert between multiple formats, among then are: wmv, avi, mpg, 3gp, mov, m4v, mts, mp4 and webm.  

The list of supported codecs are mapped on the codecs.xml file
Currently mapped codecs are:

1. Video Codecs
    + AOMedia AV1 -Matroska (Container) + AV1 (Video) + Opus (Audio)  **__Slowest__**
    + H265 HEVC - MPEG (Container) + H265 (Video) + AAC (Audio)  **__Best compression__**
    + H264 AVC - MPEG (Container) + H264 (Video) + AAC (Audio)  **__Faster and most compatible__**
    + WebM VP8 - Matroska (Container) + VP8 (Video) + Vorbis (Audio)  
    + WebM VP9 - Matroska (Container) + VP9 (Video) + Opus (Audio)  
    + Aomedia AV1 - Matroska (Container) + AV1 (Video) + Opus (Audio)  **Experimental and too slow**

1. Audio Codecs
    + MP3  
    + AAC  
    + FLAC  
    + Ogg Vorbis  

**Note:**  
The list of supported codecs may vary according the available ffmpeg codecs.

## Application usage

The application main screen, where the user can specify the source directory, select the files and the transformation option to be applied to the selected files.  

![alt text](images/main_window.png "Application main screen")

After selecting the source directories, the user should click **Refresh**; The application will search for video files in the tree of the sub-directories, displaying the video informations in the file grid: file name, file size, codec details and a check box, so the user can select the file.  
After selecting the files that should be processed, the user must click a button from the toolbar to perform the operation.  
A pop-up menu is also available for easy the selection and deletion in the file grid.  

## Features

Following is the list of features available on the application.  

### Convert

It allows to convert the selected video files to a specified codec or export the audio track.  
When clicked, a screen will be displayed with the list of codecs supported by ffmpeg that is configured inside the application.  
Upon confirmation, the application will start the conversion process and display a screen, with the progress of the activity.  
The converted file will be generated in the home screen directory and will have a prefix with the name of the codec and the extension of the codec.  

**Notes:**  

1. This action can be performed on multiple files, sequentially
2. If you select one of the audio codecs, only the audio track will be extracted.  

![alt text](images/convert_dialog.png "Video conversion screen")

### Resize

Allows you to change the resolution of the selected videos by reducing the width and height of the video file.  
A screen will appear asking for the new resolution and the output video format.  
The video resolution of the first selected video filled in by default.  

**Note:**  

1. This action can be performed on multiple files, sequentially

![alt text](images/resize_dialog.png "Video resize screen")

### Rotate

Allows the rotation and flip of the selected videos.  

The following options are available:  

+ 90 degrees clockwise
+ 90 Degrees counterclockwise  
+ 180 Degrees  
+ 90 Degrees anti-clockwise with vertical flip  
+ 90 degrees clockwise with vertical flip  
+ Flip horizontal  
+ Vertical Flip  

**Note:**  

1. This action can be performed on multiple files, sequentially

![alt text](images/rotate_dialog.png "Video rotation screen")

### Interval

This feature makes it possible to create a video from an interval of the selected video.  
A screen will be displayed with the start time and the end time of the block to be extracted and the output video format.  
The video resolution of the video is filled in by default.  

**Note:**  

1. This action is only available when a single file is selected

![alt text](images/interval_dialog.png "Interval extraction screen")

### Crop

This allows to create a video with a region of the original video (crop).  
A screen will be displayed with the initial position (vertical and horizontal), the size (width and height) and the output video format.  

**Note:**  

1. This action is only available when a single file is selected

![alt text](images/crop_dialog.png "Video crop Screen")

### Concatenate

It allows you to create a video from a sequence of videos (concatenate videos).  
A screen will be displayed with the list of videos that will be concatenated and the codec of the destination video.  
The sequence of the videos will be the sequence of the files in the text box.  

**Notes:**  

1. This action is only available when more than one file is selected
2. Always concatenate files with the same resolution
3. This feature is experimental

![alt text](images/concatenate_dialog.png "Video concatenation screen")

### Processing dialog

After confirming the operation, the application will start ffmpeg on the background and display the progress on a dialog.  

**Notes:**  

Due to the way Java handles other process, sometimes, when the user click the cancell button, the ffmpeg process is not imediatelly stopped.  
If this happens, you will need to manually stop the process on the task manager.

![alt text](images/processing_dialog.png "Processing videos dialog")

## Application log and configuration file

### System log

During execution, the application will generate a log in the `application.log` file. This file can be viewed by clicking on the `Logs` button on the home screen or by editing with any text editor.
![alt text](images/interval_dialog.png "Interval extraction screen")

### Settings file

The application settings are in the `settings.xml` file, the file has the following syntax:  

```xml
<?xml version='1.0' encoding='UTF-8'?>
<config>
    <video_extensions>wmv|avi|mpg|3gp|mov|m4v|mts|mp4|webm</video_extensions>
    <path_to_ffmpeg>ffmpeg</path_to_ffmpeg>
    <source_dir>/home/oscar/Desktop/Videos</source_dir>
    <window_x>228</window_x>
    <window_y>190</window_y>
    <window_w>1280</window_w>
    <window_h>574</window_h>
</config>
```

+ The `video_extensions` section defines the list of extensions that will be considered a video file by the application. The mapping of the extensions is case insensitive.  
+ The `path_to_ffmpeg` section indicates the path to the ffmpeg application. If this path is not specified, the application will try to use the ffmpeg on the system path.  
+ The `source_dir` section indicates the last directory used by the application.  
+ The `window_*` Indicates the applications latest size and position.

**Notes:**  
For some linux distributions (at least Ubuntu family), there are some issues regarding running process with parameters that have spaces, even when using double quotes (I know, i said Linux, not windows).  
To circunvent this bug, we recomend to configure the `path_to_ffmpeg` to use the `extras/ffmpeg_ubuntu.sh` script file.

### codecs.xml

This XML file holds the configuration for the video formats supported by the application.  

```xml
<?xml version='1.0' encoding='UTF-8'?>
<codecs>
    <!-- Video codecs -->
    <video>
        <codec name="Video H265 (HEVC)" file_suffix="_H265.mp4" requires="--enable-libx265" decoder_name="hevc">
            <params>
                <param value="-c:v" />
                <param value="libx265" />
                <param value="-c:a" />
                <param value="aac" />
            </params>
        </codec>
    </video>
    <!-- Audio codecs -->
    <audio/>
    <extras/>
```

These are the xml sections from the codecs.xml file. The **\<audio/>** and **\<video/>** sections follows the same syntax as the **\<video/>** section.  

+ The `name` section defines the video format name displayed on the application
+ The `file_suffix` section defines which extension (or suffix) the output file will have
+ The `requires` section defines a flag required by ffmpeg during compile time, that are required to allow the use of this format
+ The `decoder_name` section defines the name of the decoder (the name as ffmpeg recognize this codec)
+ The `params` section defines the arguments passed to ffmpeg , so it can convert the video to the selected format

For more information about the codecs supported by ffmpeg, check out the [ffmpeg supported codecs page](https://ffmpeg.org/ffmpeg-codecs.html) or visit the [ffmpeg codec wiki pages](https://trac.ffmpeg.org/).
