/****************************************************************************
 * CodecSettings.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils.settings;

import static videotools.utils.XMLUtils.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import videotools.utils.Constants;
import videotools.utils.to.CodecInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class CodecSettings {
    private static Logger logger = LoggerFactory.getLogger(CodecSettings.class);
    
    private static CodecSettings settings;
    private Path xmlFile;
    
    private List<CodecInfo> videoCodecs;
    private List<CodecInfo> audioCodecs;
    private List<CodecInfo> extraCodecs;
    
    /**
     * Singleton
     */
    private CodecSettings() {
        Path extrasDir = Paths.get("").resolve(Constants.DIR_NAME_EXTRAS).toAbsolutePath();
        this.xmlFile = extrasDir.resolve(Constants.CODECS_FILE_NAME).toAbsolutePath();
        this.videoCodecs = new ArrayList<>();
        this.audioCodecs = new ArrayList<>();
        this.extraCodecs = new ArrayList<>();
    }
    
    /**
     * Get the codec settings
     * 
     * @return
     */
    public static CodecSettings getInstance() {
        if (settings == null) {
            settings = new CodecSettings();
        }
        return settings;
    }
    
    /**
     * Load the codec settings
     */
    public void loadSettings() {
        if (!Files.isRegularFile(xmlFile)) {
            logger.error("Unable to read the codec settings from: {}", xmlFile.toAbsolutePath());
        } else {
            try {
                logger.info("Loading the codec settings...");
                
                Document doc = loadDocument(xmlFile);
                if (doc != null) {
                    Node rootNode = doc.getFirstChild();
                    videoCodecs = parseCodecs(rootNode, Constants.NODE_CODEC_VIDEO);
                    audioCodecs = parseCodecs(rootNode, Constants.NODE_CODEC_AUDIO);
                    extraCodecs = parseCodecs(rootNode, Constants.NODE_CODEC_EXTRAS);
                    
                }
            } catch (ParserConfigurationException | SAXException | IOException e) {
                logger.error(String.format("Failed to load the codecs.xml file: %s", e.getMessage()), e);
                e.printStackTrace();
            }
        }
    }
    
    private List<CodecInfo> parseCodecs(Node rootNode, String nodeCodecExtras) {
        List<CodecInfo> resp = new ArrayList<>();
        Node codecsNode = findNode(rootNode, nodeCodecExtras);
        if (codecsNode != null) {
            for (Node child : getRelevantChildren(codecsNode)) {
                resp.add(parseCodecInfo(child));
            }
        }
        return resp;
    }
    
    private CodecInfo parseCodecInfo(Node codecNode) {
        CodecInfo resp = new CodecInfo();
        resp.setName(getAttributeAsString(codecNode, Constants.ATTRIBUTE_NAME));
        resp.setFileSuffix(getAttributeAsString(codecNode, Constants.ATTRIBUTE_FILE_SUFFIX));
        resp.setRequires(getAttributeAsString(codecNode, Constants.ATTRIBUTE_REQUIRES));
        resp.setDecoderName(getAttributeAsString(codecNode, Constants.ATTRIBUTE_DECODER_NAME));
        
        List<String> outParms = new ArrayList<>();
        for (Node child : getRelevantChildren(findNode(codecNode, Constants.NODE_OUTPUT_PARAMS))) {
            String value = getAttributeAsString(child, Constants.ATTRIBUTE_VALUE);
            if (!StringUtils.isBlank(value)) {
                outParms.add(value);
            }
        }
        resp.setOutputParams(outParms);
        
        List<String> inParms = new ArrayList<>();
        for (Node child : getRelevantChildren(findNode(codecNode, Constants.NODE_INPUT_PARAMS))) {
            String value = getAttributeAsString(child, Constants.ATTRIBUTE_VALUE);
            if (!StringUtils.isBlank(value)) {
                inParms.add(value);
            }
        }
        resp.setInputParams(inParms);
        
        return resp;
    }
    
    /**
     * @return The current value from the field videoCodecs
     */
    public List<CodecInfo> getVideoCodecs() {
        return videoCodecs;
    }
    
    /**
     * @return The current value from the field audioCodecs
     */
    public List<CodecInfo> getAudioCodecs() {
        return audioCodecs;
    }
    
    /**
     * @return The current value from the field extraCodecs
     */
    public List<CodecInfo> getExtraCodecs() {
        return extraCodecs;
    }
    
    /**
     * Get a list with all codecs
     * 
     * @return
     */
    public List<CodecInfo> getAllCodecs() {
        List<CodecInfo> resp = new ArrayList<>();
        resp.addAll(videoCodecs);
        resp.addAll(audioCodecs);
        resp.addAll(extraCodecs);
        return resp;
    }
    
    /**
     * Get only codecs with decoders
     * 
     * @return
     */
    public List<CodecInfo> getDecoders() {
        List<CodecInfo> resp = new ArrayList<>();
        for (CodecInfo codec : videoCodecs) {
            if (!StringUtils.isBlank(codec.getDecoderName())) {
                resp.add(codec);
            }
        }
        for (CodecInfo codec : audioCodecs) {
            if (!StringUtils.isBlank(codec.getDecoderName())) {
                resp.add(codec);
            }
        }
        for (CodecInfo codec : extraCodecs) {
            if (!StringUtils.isBlank(codec.getDecoderName())) {
                resp.add(codec);
            }
        }
        return resp;
    }
}
