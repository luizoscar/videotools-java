/****************************************************************************
 * ProgramSettings.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils.settings;

import static videotools.utils.XMLUtils.*;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import videotools.utils.Constants;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class ProgramSettings {
    private static Logger logger = LoggerFactory.getLogger(ProgramSettings.class);
    private static ProgramSettings instance;
    
    private int x;
    private int y;
    private int w;
    private int h;
    private String videoExtensions;
    private String pathToFfmpeg;
    private String videosPath;
    private Path xmlFile;
    
    /**
     * Singleton
     */
    private ProgramSettings() {
        Path applicationDir = Paths.get("").toAbsolutePath();
        this.videoExtensions = Constants.DEFAULT_VIDEO_EXTENSIONS;
        this.videosPath = applicationDir.toString();
        this.xmlFile = applicationDir.resolve(Constants.SETTINGS_FILE_NAME).toAbsolutePath();
        this.pathToFfmpeg = applicationDir.resolve(Constants.DIR_NAME_EXTRAS)
                .resolve(SystemUtils.IS_OS_WINDOWS ? "ffmpeg.exe" : "ffmpeg_ubuntu.sh").toAbsolutePath().toString();
    }
    
    /**
     * Get the program settings instance
     * 
     * @return
     */
    public static ProgramSettings getInstance() {
        if (instance == null) {
            instance = new ProgramSettings();
        }
        return instance;
    }
    
    /**
     * Get the application size
     * 
     * @return The application Dimension
     */
    public Dimension getWindowSize() {
        return new Dimension(w, h);
    }
    
    /**
     * Set the current application size
     * 
     * @param d The application Dimension
     */
    public void setWindowSize(Dimension d) {
        this.w = d.width;
        this.h = d.height;
    }
    
    /**
     * Get the window position
     * 
     * @return The window coordinate
     */
    public Point getWindowPosition() {
        return new Point(x, y);
    }
    
    /**
     * Set the window position
     * 
     * @param p The window coordinate
     */
    public void setWindowPosition(Point p) {
        this.x = p.x;
        this.y = p.y;
    }
    
    /**
     * @return The current value from the field videoExtensions
     */
    public String getVideoExtensions() {
        return videoExtensions;
    }
    
    /**
     * @param videoExtensions the new value for the field videoExtensions
     */
    public void setVideoExtensions(String videoExtensions) {
        this.videoExtensions = videoExtensions;
    }
    
    /**
     * @return The current value from the field pathToFfmpeg
     */
    public String getPathToFfmpeg() {
        return pathToFfmpeg;
    }
    
    /**
     * @param pathToFfmpeg the new value for the field pathToFfmpeg
     */
    public void setPathToFfmpeg(String pathToFfmpeg) {
        this.pathToFfmpeg = pathToFfmpeg;
    }
    
    /**
     * @return The current value from the field videosPath
     */
    public String getVideosPath() {
        return videosPath;
    }
    
    /**
     * @param videosPath the new value for the field videosPath
     */
    public void setVideosPath(String videosPath) {
        this.videosPath = videosPath;
    }
    
    /**
     * Save the application settings
     */
    public void saveSettings() {
        logger.info("Saving the application settings...");
        
        try {
            Document doc = createDocument();
            Element cfgNode = createElement(doc, Constants.NODE_CONFIG);
            doc.appendChild(cfgNode);
            setElementTextValue(createElement(doc, Constants.NODE_WINDOW_H, cfgNode), Integer.toString(h));
            setElementTextValue(createElement(doc, Constants.NODE_WINDOW_W, cfgNode), Integer.toString(w));
            setElementTextValue(createElement(doc, Constants.NODE_WINDOW_X, cfgNode), Integer.toString(x));
            setElementTextValue(createElement(doc, Constants.NODE_WINDOW_Y, cfgNode), Integer.toString(y));
            setElementTextValue(createElement(doc, Constants.NODE_VIDEO_EXTENSIONS, cfgNode), StringUtils.join(videoExtensions, '|'));
            setElementTextValue(createElement(doc, Constants.NODE_SOURCE_DIR, cfgNode), videosPath);
            setElementTextValue(createElement(doc, Constants.NODE_PATH_TO_FFMPEG, cfgNode), pathToFfmpeg);
            
            Files.write(xmlFile, buildXmlText(doc, true).getBytes());
            
        } catch (ParserConfigurationException | IOException | TransformerException e) {
            logger.error(String.format("Failed to create the settings.xml file: %s", e.getMessage()), e);
            e.printStackTrace();
        }
    }
    
    /**
     * Load the application settings
     */
    public void loadSettings() {
        
        if (!Files.exists(xmlFile)) {
            logger.info("Creating the settings.xml file with the default values for the application.");
            saveSettings();
        }
        
        try {
            logger.info("Loading the application settings...");
            
            Document doc = loadDocument(xmlFile);
            if (doc != null) {
                Node root = doc.getFirstChild();
                
                this.h = getIntSettings(root, Constants.NODE_WINDOW_H);
                this.w = getIntSettings(root, Constants.NODE_WINDOW_W);
                this.x = getIntSettings(root, Constants.NODE_WINDOW_X);
                this.y = getIntSettings(root, Constants.NODE_WINDOW_Y);
                this.videoExtensions = StringUtils.defaultIfBlank(getNodeTextValue(findNode(root, Constants.NODE_VIDEO_EXTENSIONS)), this.videoExtensions);
                this.videosPath = StringUtils.defaultIfBlank(getNodeTextValue(findNode(root, Constants.NODE_SOURCE_DIR)), this.videosPath);
                this.pathToFfmpeg = StringUtils.defaultIfBlank(getNodeTextValue(findNode(root, Constants.NODE_PATH_TO_FFMPEG)), this.pathToFfmpeg);
            }
            
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error(String.format("Failed to load the settings.xml file: %s", e.getMessage()), e);
            e.printStackTrace();
        }
    }
    
    private int getIntSettings(Node root, String name) {
        return Integer.parseInt(StringUtils.getDigits(StringUtils.defaultString(getNodeTextValue(findNode(root, name)), "0")));
    }
}
