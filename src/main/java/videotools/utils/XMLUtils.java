
package videotools.utils;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class XMLUtils {
    private static Logger logger = LoggerFactory.getLogger(XMLUtils.class);
    
    private XMLUtils() {
        // Class with static methods only
    }
    
    /**
     * Create a new XML Document
     * 
     * @return The XML Document
     * @throws ParserConfigurationException If the document could not be parsed.
     */
    public static Document createDocument() throws ParserConfigurationException {
        // Create the DOC factory
        final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        dbfac.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        final DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        
        // If the file name is null, create a new document
        return docBuilder.newDocument();
    }
    
    /**
     * Load a XML Document from a XML File
     * 
     * @param file The XML file (null to create a new Document)
     * @return The XML Document
     * @throws ParserConfigurationException If the document could not be parsed.
     * @throws SAXException If SAX could not read the file
     * @throws IOException If there was an IO error
     */
    public static Document loadDocument(Path file) throws ParserConfigurationException, SAXException, IOException {
        // Create the DOC factory
        final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        dbfac.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        final DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        
        // If the file name is null, create a new document
        return docBuilder.parse(file.toFile());
    }
    
    /**
     * Load a XML Document from a XML Stream
     * 
     * @param stream The XML file stream
     * @return The XML Document
     * @throws ParserConfigurationException If the document could not be parsed.
     * @throws SAXException If SAX could not read the file
     * @throws IOException If there was an IO error
     */
    public static Document loadDocument(final InputStream stream)
            throws ParserConfigurationException, SAXException, IOException {
        // Create the DOC factory
        final DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbfac.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        dbfac.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        final DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        
        // If the file name is null, create a new document
        return (stream == null) ? docBuilder.newDocument() : docBuilder.parse(stream);
    }
    
    /**
     * Load a XML Document from a XML String
     * 
     * @param data The XML text
     * @return The XML Document
     */
    public static Document loadDocument(final String data) {
        if (StringUtils.isBlank(data)) {
            return null;
        }
        try {
            return loadDocument(new ByteArrayInputStream(data.getBytes()));
        } catch (Exception e) {
            logger.error(String.format("Failed to create the XML Document: %s ", e.getMessage()), e);
            return null;
        }
        
    }
    
    /**
     * Convert the XML Document do a String
     * 
     * @param doc The XML Document
     * @param showDeclaration true to add the XML declaration
     * @return The XML content as a String
     * @throws TransformerException If the file could not be read
     */
    public static String buildXmlText(final Document doc, boolean showDeclaration) throws TransformerException {
        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        
        // Try to set the XML indentation (some build do not support it)
        try {
            transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            transformerFactory.setAttribute("indent-number", 4);
        } catch (final Exception e) {
            // Empty
        }
        
        final Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, showDeclaration ? "no" : "yes");
        
        // Normalize the DOM tree to combine all adjacent text nodes
        doc.normalize();
        
        // create string from XML tree
        final StringWriter sw = new StringWriter();
        final StreamResult result = new StreamResult(sw);
        final DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        
        return sw.toString();
    }
    
    /**
     * Create an attribute on a XML Element
     * 
     * @param element The XML Element.
     * @param att The attribute.
     * @param value The attribute value
     * @return The modified Element
     */
    public static Element createAttribute(final Element element, final String att, final boolean value) {
        return createAttribute(element, att, Boolean.toString(value));
    }
    
    /**
     * Create an attribute on a XML Element
     * 
     * @param element The XML Element.
     * @param att The attribute.
     * @param value The attribute value
     * @return The modified Element
     */
    public static Element createAttribute(final Element element, final String att, final int value) {
        return createAttribute(element, att, Integer.toString(value));
    }
    
    /**
     * Create an attribute on a XML Element
     * 
     * @param element The XML Element.
     * @param att The attribute.
     * @param value The attribute value
     * @return The modified Element
     */
    public static Element createAttribute(final Element element, final String att, final String value) {
        if (value != null && value.length() > 0) {
            element.setAttribute(att, value);
        }
        return element;
    }
    
    /**
     * Get the value from a attribute as a String
     * 
     * @param node The XML Node
     * @param name The attribute name
     * @return The XML value
     */
    public static String getAttributeAsString(final Node node, final String name) {
        return getAttributesMap(node).get(name.toLowerCase());
    }
    
    /**
     * Get the value from a attribute as Boolean
     * 
     * @param node The XML Node
     * @param name The attribute name
     * @return The XML value
     */
    public static boolean getAttributeAsBoolean(final Node node, final String name) {
        return Boolean.parseBoolean(getAttributeAsString(node, name));
    }
    
    /**
     * Get the value from a attribute as a int
     * 
     * @param node The XML Node
     * @param name The attribute name
     * @return The XML value
     */
    
    public static int getAttributeAsInt(final Node node, final String name) {
        return Utils.getTextNumber(getAttributeAsString(node, name));
    }
    
    /**
     * Get the value from a attribute as a Float
     * 
     * @param node The XML Node
     * @param name The attribute name
     * @return The XML value
     */
    public static float getAttributeAsFloat(final Node node, final String name) {
        return Utils.getTextNumberFloat(getAttributeAsString(node, name));
    }
    
    /**
     * Build a tree with all attributes from a Node
     * 
     * @param node The XML Node
     * @return The attribute map
     */
    public static Map<String, String> getAttributesMap(final Node node) {
        final Map<String, String> resp = new HashMap<>();
        
        if (node != null) {
            // Get all attributes from the given node
            final NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                // Try to map all the attributes
                Node temp = null;
                for (int i = 0; i < attributes.getLength(); i++) {
                    if ((temp = attributes.item(i)) != null) {
                        // Get the attribute
                        resp.put(temp.getNodeName().toLowerCase(), temp.getNodeValue());
                    }
                }
            }
        }
        return resp;
    }
    
    /**
     * Build a tree with all attributes from a XML String
     * 
     * @param line The XML line
     * @return The attribute map
     */
    public static Map<String, String> getAttributesMap(final String line) {
        final Map<String, String> resp = new HashMap<>();
        
        if (StringUtils.isNotBlank(line)) {
            String temp = line.trim();
            while (StringUtils.contains(temp, '=')) {
                // Get the attribute name
                String name = StringUtils.substringBefore(temp, "=").trim();
                // Get the attribute value
                temp = StringUtils.substringAfter(temp, "\"");
                final String value = StringUtils.substringBefore(temp, "\"");
                temp = StringUtils.substringAfter(temp, "\"");
                // Add the item to the map
                resp.put(name, value);
            }
        }
        return resp;
    }
    
    /**
     * Create a new XML Element on a existing Document
     * 
     * @param doc The XML Document
     * @param name The new Element name
     * @return The new XML Element
     */
    public static Element createElement(final Document doc, final String name) {
        return doc.createElement(name);
    }
    
    /**
     * Create a new XML Element as child of an existing Element
     * 
     * @param doc The XML Document
     * @param name The new Element name
     * @param parent The parent Element
     * @return The new XML Element
     */
    public static Element createElement(final Document doc, final String name, final Node parent) {
        final Element resp = createElement(doc, name);
        parent.appendChild(resp);
        return resp;
    }
    
    /**
     * Build a list with all XML Nodes inside a parent Node
     * 
     * @param n The parent node
     * @return The list of Nodes
     */
    public static List<Node> getChildren(final Node n) {
        final List<Node> result = new ArrayList<>();
        if (n != null) {
            final NodeList childs = n.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                final Node child = childs.item(i);
                if (child != null) {
                    switch (child.getNodeType()) {
                        case Node.ELEMENT_NODE:
                        case Node.COMMENT_NODE:
                            result.add(child);
                            break;
                        case Node.TEXT_NODE:
                            String v = child.getNodeValue();
                            if (StringUtils.isNotBlank(v)) {
                                result.add(child);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Build a list with all relevant XML Nodes inside a parent Node, ignoring comments
     * 
     * @param n The parent node
     * @return The list of Nodes
     */
    public static List<Node> getRelevantChildren(final Node parent) {
        final List<Node> result = new ArrayList<>();
        for (Node child : getChildren(parent)) {
            switch (child.getNodeType()) {
                case Node.ELEMENT_NODE:
                    result.add(child);
                    break;
                case Node.TEXT_NODE:
                    String v = child.getNodeValue();
                    if (StringUtils.isNotBlank(v)) {
                        result.add(child);
                    }
                    break;
                default:
                    break;
            }
        }
        return result;
    }
    
    /**
     * Get the text value from a Node
     * 
     * @param node The node to get the text value
     * @return The node value, null if the node was null
     */
    public static String getNodeTextValue(Node node) {
        return node == null ? null : node.getTextContent();
    }
    
    /**
     * Set the value from a Element
     * 
     * @param node The XML Element
     * @param textValue The text value
     * @return The modified Element
     */
    public static Element setElementTextValue(Element node, String textValue) {
        if (node != null) {
            node.setTextContent(textValue);
        }
        return node;
    }
    
    /**
     * Locate a XML node by his name
     * 
     * @param name The node name
     * @param node The start node
     * @return The located Node, null if not found
     */
    public static Node findNode(final Node node, final String name) {
        if (node != null) {
            if (StringUtils.endsWithIgnoreCase(name, node.getNodeName())) {
                return node;
            }
            
            // Retrieve a list with all root child nodes
            for (final Node n : getChildren(node)) {
                
                if (StringUtils.equalsIgnoreCase(name, n.getNodeName())) {
                    return n;
                } else {
                    final Node temp = findNode(n, name);
                    if (temp != null) {
                        return temp;
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Clone a XML Element.
     * 
     * @param doc The XML document.
     * @param node The source XML Element.
     * @return The cloned element
     */
    public static Node cloneElement(final Document doc, final Node node) {
        
        if (node.getNodeType() == Node.TEXT_NODE && StringUtils.isNotBlank(node.getNodeValue())) {
            return doc.createTextNode(node.getNodeValue());
        }
        if (node.getNodeType() == Node.COMMENT_NODE) {
            Comment c = (Comment) node;
            return doc.createComment(c.getData());
        }
        
        final Element newNode = doc.createElement(node.getNodeName());
        
        final NamedNodeMap attributes = node.getAttributes();
        if (attributes != null) {
            Node temp = null;
            for (int i = 0; i < attributes.getLength(); i++) {
                if ((temp = attributes.item(i)) != null) {
                    final String name = temp.getNodeName();
                    final String value = temp.getNodeValue();
                    newNode.setAttribute(name, value);
                }
            }
        }
        
        return newNode;
    }
    
    /**
     * Cone a XML tree, copying all attributes and children
     * 
     * @param doc The XML document.
     * @param node The source XML Element.
     * @return The cloned element
     */
    public static Node createClone(final Document doc, final Node parent) {
        final Node newNode = cloneElement(doc, parent);
        getChildren(parent).stream().forEach(child -> newNode.appendChild(createClone(doc, child)));
        return newNode;
    }
}
