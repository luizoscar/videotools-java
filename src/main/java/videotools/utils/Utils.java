/****************************************************************************
 * Utils.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.apache.commons.lang3.StringUtils;

import videotools.utils.settings.CodecSettings;
import videotools.utils.to.CodecInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class Utils {
    private Utils() {
        // Class with static methods only
    }
    
    /**
     * Convert a string to integer
     * 
     * @param line The string containing the number
     * @return the converted number
     */
    public static int getTextNumber(final String line) {
        String value = (StringUtils.contains(line, "-") ? "-0" : "0") + StringUtils.getDigits(line);
        return Integer.parseInt(value);
    }
    
    /**
     * Convert a string to float
     * 
     * @param line The string containing the number
     * @return the converted number
     */
    public static float getTextNumberFloat(final String line) {
        final StringBuilder tmp = new StringBuilder("0");
        if (StringUtils.isNotBlank(line)) {
            final char[] chars = StringUtils.trimToEmpty(line).toCharArray();
            final boolean isNegative = StringUtils.contains(line, '-');
            boolean isFloat = false;
            final char floatChar = '.';
            
            // Find all numbers on the string
            for (final char c : chars) {
                if (Character.isDigit(c)) {
                    tmp.append(c);
                } else {
                    if ((c == floatChar) && (!isFloat)) {
                        isFloat = true;
                        tmp.append(c);
                    }
                }
            }
            
            if (isNegative) {
                tmp.insert(0, '-');
            }
        }
        return Float.parseFloat(tmp.toString());
    }
    
    /**
     * Get the byte count in human readable format
     * 
     * @param bytes The number of bytes
     * @return The size in formatted to human
     */
    public static String getFormattedByteCount(long bytes) {
        int unit = 1024;
        if (bytes < unit) {
            return bytes + " B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = ("KMGTPE").charAt(exp - 1) + "";
        return String.format(new Locale("pt", "BR"), "%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
    
    /**
     * Get the number of seconds since a time
     * 
     * @param since
     * @return
     */
    public static double getNumberOfSeconds(long since) {
        return (System.currentTimeMillis() - since) / 1000.0;
    }
    
    /**
     * Convert to seconds a string in the format HH:MM:SS
     * 
     * @param time The time String
     * @return The number of seconds
     */
    public static int getNumberOfSeconds(String time) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        int resp = 0;
        if (!StringUtils.isBlank(time)) {
            try {
                Date reference = df.parse("00:00:00");
                Date date = df.parse(time);
                resp = (int) ((date.getTime() - reference.getTime()) / 1000L);
            } catch (ParseException e) {
                resp = 0;
            }
        }
        return resp;
    }
    
    /**
     * Format a time in seconds to hh:mm:ss
     * 
     * @param totalSecs The number of seconds
     * @return The formatted time
     */
    public static String getTimeFromSeconds(int totalSecs) {
        long hours = totalSecs / 3600;
        long minutes = (totalSecs % 3600) / 60;
        long seconds = totalSecs % 60;
        
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
    
    /**
     * Create a new menu item
     * 
     * @param title The item title
     * @param l The ActionListenet
     * @return
     */
    public static JMenuItem createMenuItem(String title, ActionListener l) {
        JMenuItem item = new JMenuItem(title);
        item.addActionListener(l);
        return item;
    }
    
    /**
     * Create a new button
     * 
     * @param title The button title
     * @param icon The icon name
     * @return
     */
    public static JButton createButton(String title, String icon) {
        JButton resp = new JButton(title);
        try {
            Image image = ImageIO.read(Utils.class.getResource("/icons/" + icon));
            resp.setIcon(new ImageIcon(image));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        resp.setHorizontalAlignment(SwingConstants.LEFT);
        return resp;
    }
    
    public static JComboBox<CodecInfo> buildCodecCombo() {
        JComboBox<CodecInfo> cbCodecs = new JComboBox<>();
        CodecSettings.getInstance().getAllCodecs().forEach(cbCodecs::addItem);
        return cbCodecs;
    }
    
    public static GridBagConstraints buildConstraints(int anchor, int x, int y) {
        GridBagConstraints c0 = new GridBagConstraints();
        c0.anchor = anchor;
        c0.fill = GridBagConstraints.HORIZONTAL;
        c0.insets = new Insets(5, 5, 5, 5);
        c0.weightx = 1;
        c0.gridx = x;
        c0.gridy = y;
        return c0;
    }
    
    /**
     * Display an Error message tied to "this" - Modal
     * 
     * @param message The message that shall be displayed
     * @param parent The parent frame
     */
    public static void showModalMessageError(final Component owner, final String message) {
        JOptionPane.showMessageDialog(owner, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * Display an information message tied to "this" - Modal
     * 
     * @param message The message that shall be displayed
     * @param parent The parent frame
     */
    public static void showModalMessageInformation(final Component owner, final String message) {
        JOptionPane.showMessageDialog(owner, message, "Information", JOptionPane.INFORMATION_MESSAGE);
    }
    
    /**
     * Display an exclamation message tied to "this" - Modal
     * 
     * @param message The message that shall be displayed
     * @param parent The parent frame
     */
    public static void showModalMessageWarning(final Component owner, final String message) {
        JOptionPane.showMessageDialog(owner, message, "Warning", JOptionPane.WARNING_MESSAGE);
    }
    
    /**
     * Display a question messages tied to "this" - Modal
     * 
     * @return boolean If the user has answered YES
     * @param message The message that shall be displayed
     * @param parent The parent frame
     */
    public static boolean askQuestion(final Component owner, final String message) {
        return (JOptionPane.showConfirmDialog(owner, message, "Question",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
    }
    
    /**
     * Open a dialog window for the user select a file
     * 
     * @return File The selected file name (null if dialog was canceled)
     * @param title The message that shall be displayed
     * @param parent The parent frame
     * @param startFolder The folder that should be opened by default (optional)
     */
    public static File openDirDialog(final Component parent, final String title, final String startFolder) {
        
        final JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setDialogTitle(title);
        
        if (!StringUtils.isBlank(startFolder)) {
            final File curDir = new File(startFolder);
            fc.setCurrentDirectory(curDir);
        }
        
        final int returnVal = fc.showOpenDialog(parent);
        File selFile = fc.getSelectedFile();
        
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            if (selFile.isFile()) {
                selFile = selFile.getParentFile();
            }
            return selFile;
        } else {
            return null;
        }
    }
}