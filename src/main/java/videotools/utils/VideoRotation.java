/****************************************************************************
 * VideoRotation.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public enum VideoRotation {
    
    ROTATE_90_CW("90 Degree clockwise", "transpose=1"),
    ROTATE_90_CCW("90 Degree counter-clockwise", "transpose=2"),
    ROTATE_180("180 Degree", "transpose=2,transpose=2"),
    
    ROTATE_90_CW_V_FLIP("90 Degree counter-clockwise with vertical flip", "transpose=0"),
    ROTATE_90_CW_H_FLIP("90 degree clockwise with vertical flip", "transpose=3"),
    
    FLIP_HORIZONTAL("Horizontal flip", "hflip"),
    FLIP_VERTICAL("Vertical flip", "vflip");
    
    private String title;
    private String filter;
    
    private VideoRotation(String title, String filter) {
        this.title = title;
        this.filter = filter;
    }
    
    @Override
    public String toString() {
        return title;
    }
    
    public String getFilter() {
        return filter;
    }
}
