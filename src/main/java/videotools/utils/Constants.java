/****************************************************************************
 * Constants.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils;

import java.util.regex.Pattern;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public final class Constants {
    private Constants() {
        // Empty
    }
    
    public static final String REGEX_DURATION = "(Duration: [0-9]{2,}:[0-9]{2,}:[0-9]{2,})";
    public static final String REGEX_VIDEO = "(Video: [^\\s]+)|([0-9]{2,}x[0-9]{2,})|([0-9|.]+ fps)";
    public static final String REGEX_AUDIO = "(Audio: [^\\s]+)|([0-9]+ Hz)";
    
    public static final Pattern PATTERN_RESOLUTION = Pattern.compile("([0-9]{2,})[xX]([0-9]{2,})");
    public static final Pattern PATTERN_TIME = Pattern.compile("[0-9]{2,}:[0-9]{2}:[0-9]{2}");
    public static final String TITLE = "VideoTools";
    public static final int DEFAULT_WIDTH = 1100;
    public static final int DEFAULT_HEIGHT = 500;
    public static final String LOG_FILE_NAME = "output.log";
    public static final String SETTINGS_FILE_NAME = "settings.xml";
    public static final String CODECS_FILE_NAME = "codecs.xml";
    public static final String DIR_NAME_EXTRAS = "extras";
    public static final String DEFAULT_VIDEO_EXTENSIONS = "wmv|avi|mpg|3gp|mov|m4v|mts|mp4|webm|mkv";
    
    // Icon names
    public static final String ICON_OPEN = "open.png";
    public static final String ICON_REFRESH = "refresh.png";
    public static final String ICON_CONVERT = "convert.png";
    public static final String ICON_RESIZE = "resize.png";
    public static final String ICON_ROTATE = "rotate.png";
    public static final String ICON_INTERVAL = "interval.png";
    public static final String ICON_CROP = "crop.png";
    public static final String ICON_CONCATENATE = "concatenate.png";
    public static final String ICON_LOGS = "logs.png";
    public static final String ICON_EXIT = "close.png";
    public static final String ICON_OK = "ok.png";
    
    // Columns
    public static final String COL_NAME_SELECTED = "Selected";
    public static final String COL_NAME_FILE = "File";
    public static final String COL_NAME_SIZE = "Size";
    public static final String COL_NAME_DURATION = "Duration";
    public static final String COL_NAME_VIDEO = "Video";
    public static final String COL_NAME_AUDIO = "Audio";
    
    // XML Nodes
    public static final String NODE_VIDEO_EXTENSIONS = "video_extensions";
    public static final String NODE_PATH_TO_FFMPEG = "path_to_ffmpeg";
    public static final String NODE_SOURCE_DIR = "source_dir";
    public static final String NODE_WINDOW_X = "window_x";
    public static final String NODE_WINDOW_Y = "window_y";
    public static final String NODE_WINDOW_W = "window_w";
    public static final String NODE_WINDOW_H = "window_h";
    public static final String NODE_CONFIG = "config";
    public static final String NODE_CODEC_VIDEO = "video";
    public static final String NODE_CODEC_AUDIO = "audio";
    public static final String NODE_CODEC_EXTRAS = "extra";
    public static final String NODE_OUTPUT_PARAMS = "output_params";
    public static final String NODE_INPUT_PARAMS = "input_params";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String ATTRIBUTE_FILE_SUFFIX = "file_suffix";
    public static final String ATTRIBUTE_REQUIRES = "requires";
    public static final String ATTRIBUTE_DECODER_NAME = "decoder_name";
    public static final String ATTRIBUTE_VALUE = "value";
}
