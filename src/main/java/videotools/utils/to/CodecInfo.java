/****************************************************************************
 * CodecInfo.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class CodecInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String fileSuffix;
    private String requires;
    private String decoderName;
    private List<String> inputParams = new ArrayList<>();
    private List<String> outputParams = new ArrayList<>();
    
    /**
     * @return The current value from the field name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @param name the new value for the field name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return The current value from the field fileSuffix
     */
    public String getFileSuffix() {
        return fileSuffix;
    }
    
    /**
     * @param fileSuffix the new value for the field fileSuffix
     */
    public void setFileSuffix(String fileSuffix) {
        this.fileSuffix = fileSuffix;
    }
    
    /**
     * @return The current value from the field requires
     */
    public String getRequires() {
        return requires;
    }
    
    /**
     * @param requires the new value for the field requires
     */
    public void setRequires(String requires) {
        this.requires = requires;
    }
    
    /**
     * @return The current value from the field decoderName
     */
    public String getDecoderName() {
        return decoderName;
    }
    
    /**
     * @param decoderName the new value for the field decoderName
     */
    public void setDecoderName(String decoderName) {
        this.decoderName = decoderName;
    }
    
    /**
     * @return The current value from the field inputParams
     */
    public List<String> getInputParams() {
        return inputParams;
    }
    
    /**
     * @param inputParams the new value for the field inputParams
     */
    public void setInputParams(List<String> inputParams) {
        this.inputParams = inputParams;
    }
    
    /**
     * @return The current value from the field outputParams
     */
    public List<String> getOutputParams() {
        return outputParams;
    }
    
    /**
     * @param outputParams the new value for the field outputParams
     */
    public void setOutputParams(List<String> outputParams) {
        this.outputParams = outputParams;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
