/****************************************************************************
 * VideoInfo.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.utils.to;

import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import videotools.utils.Constants;
import videotools.utils.Utils;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class VideoInfo {
    long bytes;
    Path path;
    FileTime lastModified;
    private String formattedDuration;
    private String formattedVideo;
    private String formattedAudio;
    
    /**
     * @return The current value from the field bytes
     */
    public long getBytes() {
        return bytes;
    }
    
    /**
     * @param bytes the new value for the field bytes
     */
    public void setBytes(long bytes) {
        this.bytes = bytes;
    }
    
    /**
     * @return The current value from the field path
     */
    public Path getPath() {
        return path;
    }
    
    /**
     * @param path the new value for the field path
     */
    public void setPath(Path path) {
        this.path = path;
    }
    
    /**
     * @return The current value from the field lastModified
     */
    public FileTime getLastModified() {
        return lastModified;
    }
    
    /**
     * @param lastModified the new value for the field lastModified
     */
    public void setLastModified(FileTime lastModified) {
        this.lastModified = lastModified;
    }
    
    /**
     * Return the abbreviated file name
     * 
     * @param parentDir The parent dir
     * @return
     */
    public String getAbbreviatedName(String parentDir) {
        return StringUtils.substring(path.toAbsolutePath().toString(), parentDir.length() + 1);
    }
    
    /**
     * get the amount of time in seconds
     * 
     * @return The number of seconds
     */
    public int getNumberOfSeconds() {
        return Utils.getNumberOfSeconds(this.formattedDuration);
    }
    
    /**
     * Get the formatted audio details
     * 
     * @return the audio details
     */
    public String getFormattedAudioCodec() {
        return this.formattedAudio;
    }
    
    public String getFormattedVideoDuration() {
        return this.formattedDuration;
    }
    
    public String getFormattedVideoCodec() {
        return this.formattedVideo;
    }
    
    public void setDetails(String details) {
        this.formattedDuration = StringUtils.defaultString(StringUtils.substringAfter(extractDetail(Constants.REGEX_DURATION, details), " "));
        this.formattedVideo = StringUtils.defaultString(StringUtils.substringAfter(extractDetail(Constants.REGEX_VIDEO, details), " "));
        this.formattedAudio = StringUtils.defaultString(StringUtils.substringAfter(extractDetail(Constants.REGEX_AUDIO, details), " "));
    }
    
    /**
     * Get the size of the video file in MB
     * 
     * @return
     */
    public String getFormattedVideoFileSize() {
        return Utils.getFormattedByteCount(bytes);
    }
    
    @Override
    public String toString() {
        return getFormattedAudioCodec();
    }
    
    private String extractDetail(String regex, String details) {
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(details);
        StringBuilder bld = new StringBuilder("");
        while (m.find()) {
            bld.append(m.group()).append(" ");
        }
        return bld.toString();
    }
}
