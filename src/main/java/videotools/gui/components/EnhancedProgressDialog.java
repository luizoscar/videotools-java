/****************************************************************************
 * EnhancedProgressDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.components;

import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import videotools.utils.Constants;
import videotools.utils.Utils;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class EnhancedProgressDialog extends JDialog implements Runnable {
    private static final long serialVersionUID = 1L;
    private static final String FRAME = "frame=";
    private static final String TIME = "time=";
    
    protected static Logger logger = LoggerFactory.getLogger(EnhancedProgressDialog.class);
    
    private JLabel labelJobDescription;
    
    private JLabel labelGlobalStatus;
    private JProgressBar globalProgressBar;
    
    private JLabel labelCurrentStatus;
    private JProgressBar currentProgressBar;
    private transient Path outputVideoFile;
    
    private long totalBytes;
    private int totalSecs;
    private int processedSecs;
    
    private long outputVideoBytes;
    private int outputVideoSecs;
    private int inputVideoSecs;
    
    private boolean threadActive;
    private CodecInfo ouputCodec;
    private transient List<String> processlines;
    private transient List<VideoInfo> videosToProcess;
    
    public EnhancedProgressDialog(JFrame parent, String title, boolean videoProcessorTask,
            List<VideoInfo> videosToProcess, CodecInfo ouputCodec) {
        super(parent, title, true);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setSize(500, videoProcessorTask ? 200 : 150);
        this.setLocationRelativeTo(parent);
        this.setResizable(false);
        this.videosToProcess = videosToProcess == null ? new ArrayList<>() : videosToProcess;
        this.ouputCodec = ouputCodec;
        for (VideoInfo info : this.videosToProcess) {
            setTotalSecs(getTotalSecs() + info.getNumberOfSeconds());
            setTotalBytes(getTotalBytes() + info.getBytes());
        }
        
        JPanel paCenter = new JPanel(new GridLayout(videoProcessorTask ? 5 : 3, 1, 10, 10));
        // Job description
        labelJobDescription = new JLabel("Reading files from:", SwingConstants.LEFT);
        paCenter.add(labelJobDescription);
        
        if (videoProcessorTask) {
            // Global Progress bar
            globalProgressBar = new JProgressBar();
            paCenter.add(globalProgressBar);
            
            // Global Status
            labelGlobalStatus = new JLabel("Checking file:", SwingConstants.LEFT);
            paCenter.add(labelGlobalStatus);
        }
        
        // Current Progress bar
        currentProgressBar = new JProgressBar();
        paCenter.add(currentProgressBar);
        
        // Current Status
        labelCurrentStatus = new JLabel("Checking file:", SwingConstants.LEFT);
        paCenter.add(labelCurrentStatus);
        
        // Cancel
        JButton btClose = Utils.createButton("Cancel", Constants.ICON_EXIT);
        btClose.addActionListener(e -> {
            threadActive = false;
            setVisible(false);
        });
        JPanel paBottom = new JPanel(new FlowLayout());
        paBottom.add(btClose);
        
        JPanel paMain = new JPanel(new BorderLayout(5, 5));
        paMain.add(paCenter, BorderLayout.CENTER);
        paMain.add(paBottom, BorderLayout.SOUTH);
        
        setContentPane(new JPanel(new GridBagLayout()));
        GridBagConstraints c0 = new GridBagConstraints();
        c0.anchor = GridBagConstraints.CENTER;
        c0.fill = GridBagConstraints.BOTH;
        c0.insets = new Insets(5, 5, 5, 5);
        c0.weightx = 1;
        add(paMain, c0);
    }
    
    /**
     * @return The current value from the field ouputCodec
     */
    public CodecInfo getOuputCodec() {
        return ouputCodec;
    }
    
    /**
     * Set the task description
     * 
     * @param title The task title
     */
    protected void setJobDescription(String title) {
        labelJobDescription.setText(title);
    }
    
    /**
     * Set the global status
     */
    protected void setGlobalStatus(String title) {
        labelGlobalStatus.setText(title);
    }
    
    /**
     * Set the current status
     */
    protected void setCurrentStatus(String title) {
        labelCurrentStatus.setText(title);
    }
    
    /**
     * Set the task global progress
     * 
     * @param min The minimum task size
     * @param max The maximum task size
     * @param current The current step
     */
    protected void setGlobalProgress(int min, int max, int current) {
        globalProgressBar.setMinimum(min);
        globalProgressBar.setValue(current);
        globalProgressBar.setMaximum(max);
    }
    
    /**
     * Set the task current progress
     * 
     * @param min The minimum task size
     * @param max The maximum task size
     * @param current The current step
     */
    protected void setCurrentProgress(int min, int max, int current) {
        currentProgressBar.setMinimum(min);
        currentProgressBar.setValue(current);
        currentProgressBar.setMaximum(max);
    }
    
    /**
     * Start the task and show the dialog
     */
    public void showAndExecuteTask() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        this.threadActive = true;
        new Thread(this).start();
        
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setVisible(true);
    }
    
    /**
     * This is the method that will loop on the selected videos. This method will call processVideo() for each video.
     */
    
    @Override
    public void run() {
        long ini = System.currentTimeMillis();
        int index = 1;
        for (VideoInfo videoInfo : getVideosToProcess()) {
            String msg = String.format("Processing video [%d / %d] - (%s) - %s", index, getVideosToProcess().size(),
                    Utils.getFormattedByteCount(getTotalBytes()), Utils.getTimeFromSeconds(getTotalSecs()));
            
            logger.info("------------------------------------------------------------------");
            logger.info(msg);
            setJobDescription(msg);
            
            processVideo(videoInfo);
            index++;
        }
        logger.info("------------------------------------------------------------------");
        logger.info("Processed {} videos in {} sec", getVideosToProcess().size(), Utils.getNumberOfSeconds(ini));
        dispose();
    }
    
    /**
     * Method to be overridden to perform each video processing technique
     * 
     * @param info The video to be processed
     */
    protected void processVideo(VideoInfo info) {
        throw new IllegalArgumentException("This method must be overriden on the top class!");
    }
    
    /**
     * Mark a task as with an indeterminated size
     * 
     * @param b
     */
    protected void setGlobalIndeterminate(boolean b) {
        globalProgressBar.setIndeterminate(b);
        
    }
    
    /**
     * Mark a task as with an indeterminated size
     * 
     * @param b
     */
    protected void setCurrentIndeterminate(boolean b) {
        currentProgressBar.setIndeterminate(b);
        
    }
    
    /**
     * Check if the user cancelled the operation
     * 
     * @return true if thre threa must be interrupted
     */
    public boolean wasCanceled() {
        return !this.threadActive;
    }
    
    /**
     * @return The current value from the field outputVideoFile
     */
    public Path getOutputVideoFile() {
        return outputVideoFile;
    }
    
    /**
     * @param outputVideoFile the new value for the field outputVideoFile
     */
    public void setOutputVideoFile(Path outputVideoFile) {
        this.outputVideoFile = outputVideoFile;
    }
    
    public void buildOutputVideoFile(Path sourceVideoFile, String suffix) throws IOException {
        String nameWithouExtension = StringUtils.substringBeforeLast(sourceVideoFile.getFileName().toString(), ".");
        this.outputVideoFile = sourceVideoFile.getParent().resolve(nameWithouExtension + suffix);
        logger.info("Converting file: {} -> {} ", sourceVideoFile.toAbsolutePath(), outputVideoFile.toAbsolutePath());
        
        // Override existing file
        if (Files.isRegularFile(outputVideoFile)) {
            logger.info("Overriding existing file: {} ", outputVideoFile.toAbsolutePath());
            Files.delete(outputVideoFile.toAbsolutePath());
        }
    }
    
    /**
     * @return The current value from the field totalBytes
     */
    public long getTotalBytes() {
        return totalBytes;
    }
    
    /**
     * @param totalBytes the new value for the field totalBytes
     */
    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }
    
    /**
     * @return The current value from the field totalSecs
     */
    public int getTotalSecs() {
        return totalSecs;
    }
    
    /**
     * @param totalSecs the new value for the field totalSecs
     */
    public void setTotalSecs(int totalSecs) {
        this.totalSecs = totalSecs;
    }
    
    /**
     * @return The current value from the field processedSecs
     */
    public int getProcessedSecs() {
        return processedSecs;
    }
    
    /**
     * @param processedSecs the new value for the field processedSecs
     */
    public void setProcessedSecs(int processedSecs) {
        this.processedSecs = processedSecs;
    }
    
    /**
     * @return The current value from the field outputVideoBytes
     */
    public long getOutputVideoBytes() {
        return outputVideoBytes;
    }
    
    /**
     * @param outputVideoBytes the new value for the field outputVideoBytes
     */
    public void setOutputVideoBytes(long outputVideoBytes) {
        this.outputVideoBytes = outputVideoBytes;
    }
    
    /**
     * @return The current value from the field outputVideoSecs
     */
    public int getOutputVideoSecs() {
        return outputVideoSecs;
    }
    
    /**
     * @param outputVideoSecs the new value for the field outputVideoSecs
     */
    public void setOutputVideoSecs(int outputVideoSecs) {
        this.outputVideoSecs = outputVideoSecs;
    }
    
    /**
     * @return The current value from the field inputVideoSecs
     */
    public int getInputVideoSecs() {
        return inputVideoSecs;
    }
    
    /**
     * @param inputVideoSecs the new value for the field inputVideoSecs
     */
    public void setInputVideoSecs(int inputVideoSecs) {
        this.inputVideoSecs = inputVideoSecs;
    }
    
    /**
     * @return The current value from the field videosToProcess
     */
    public List<VideoInfo> getVideosToProcess() {
        return videosToProcess;
    }
    
    /**
     * Check if a file is a video
     * 
     * @param path The file path
     * @param extensions Video file extensions
     * @return
     */
    public boolean isVideo(Path path, final String extensions) {
        String ext = StringUtils.lowerCase(StringUtils.substringAfterLast(path.toString(), '.'));
        return Files.isRegularFile(path) && !StringUtils.isBlank(ext) && StringUtils.contains(extensions, ext);
    }
    
    /**
     * Parse the output lines from the running process
     * 
     * @param line The current output line
     */
    protected void parseLine(String line) {
        // Ignore useless lines
        if (StringUtils.contains(line, FRAME) && StringUtils.contains(line, TIME)) {
            outputVideoSecs = getProcessedTime(line);
            try {
                outputVideoBytes = Files.size(outputVideoFile);
            } catch (IOException e) {
                logger.error(String.format("Failed to get the file size from %s: %s",
                        outputVideoFile.toAbsolutePath().toString(), e.getMessage()), e);
            }
            // Update the progress
            setCurrentStatus(String.format("To: %s - %s (%s)", outputVideoFile.getFileName(),
                    Utils.getTimeFromSeconds(outputVideoSecs), Utils.getFormattedByteCount(outputVideoBytes)));
            setCurrentProgress(0, inputVideoSecs, outputVideoSecs);
            setGlobalProgress(0, totalSecs, processedSecs + outputVideoSecs);
        }
    }
    
    /**
     * Convert the processes time from hh:mm:ss -> secs
     * 
     * @param line The line containing the progress
     * @return
     */
    private int getProcessedTime(String line) {
        String tmp = StringUtils.substringAfter(line, TIME);
        return Utils.getNumberOfSeconds(StringUtils.substringBefore(tmp, ","));
    }
    
    /**
     * Execute a shell process
     * 
     * @param applicationPath The application path
     * @param exitValue The expected exit value
     * @param params The process arguments
     * @throws IOException
     */
    protected List<String> executeProcess(String applicationPath, int exitValue, String... params) throws IOException {
        processlines = new ArrayList<>();
        CommandLine cmdLine = new CommandLine(applicationPath);
        // Note: Some linux distros are having problems with quotes
        cmdLine.addArguments(params, SystemUtils.IS_OS_WINDOWS);
        String msg = String.format("Running: %s %s", applicationPath, StringUtils.join(params, " "));
        logger.info(msg);
        
        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(exitValue);
        
        OutputStream outHandler = new OutputStream() {
            private StringBuilder buff = new StringBuilder();
            
            @Override
            public void write(int b) throws IOException {
                char c = (char) b;
                if (CharUtils.isAsciiPrintable(c)) {
                    this.buff.append(c);
                } else {
                    String line = buff.toString();
                    logger.info(line);
                    processlines.add(line);
                    parseLine(line);
                    buff.delete(0, buff.length());
                }
                // Check if the user has pressed the cancel button
                if (wasCanceled()) {
                    executor.getWatchdog().destroyProcess();
                }
            }
        };
        
        executor.setStreamHandler(new PumpStreamHandler(outHandler, outHandler));
        executor.execute(cmdLine);
        return processlines;
    }
}
