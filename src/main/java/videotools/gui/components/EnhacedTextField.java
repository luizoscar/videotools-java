
package videotools.gui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class EnhacedTextField extends JPanel {
    private static final long serialVersionUID = 1L;
    
    JTextField editor = new JTextField();
    
    /**
     * 'Cut' JPopup menu item
     */
    JMenuItem jMenuCut = new JMenuItem("Cut");
    
    /**
     * 'Copy' JPopup menu item
     */
    JMenuItem jMenuCopy = new JMenuItem("Copy");
    
    /**
     * 'Paste' JPopup menu item
     */
    JMenuItem jMenuPaste = new JMenuItem("Paste");
    
    /**
     * 'Select All' JPopup menu item
     */
    JMenuItem jMenuSelectAll = new JMenuItem("Select All");
    
    /**
     * 'Copy All' JPopup menu item
     */
    JMenuItem jMenuCopyAll = new JMenuItem("Copy All");
    
    /**
     * The editor JPopup menu
     */
    private JPopupMenu popupMenu = null;
    
    /**
     * Default constructor
     */
    public EnhacedTextField() {
        super();
        initialize();
    }
    
    /**
     * Default constructor
     */
    public EnhacedTextField(String text) {
        this();
        setText(text);
    }
    
    /**
     * Initialize the editor, JPopup
     */
    private void initialize() {
        /*
         * Add The Undo and Redo functionalities to the editor
         */
        final UndoManager undo = new UndoManager();
        Document doc = editor.getDocument();
        
        // Listen for undo and redo events
        doc.addUndoableEditListener(evt -> undo.addEdit(evt.getEdit()));
        
        // Create an undo action and add it to the text component
        editor.getActionMap().put("Undo", new AbstractAction("Undo") {
            private static final long serialVersionUID = 1L;
            
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (undo.canUndo()) {
                        undo.undo();
                    }
                } catch (CannotUndoException e) {
                    // Ignore
                }
            }
        });
        
        // Bind the undo action to CTRL-Z
        editor.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
        
        // Create a redo action and add it to the text component
        editor.getActionMap().put("Redo", new AbstractAction("Redo") {
            private static final long serialVersionUID = 1L;
            
            public void actionPerformed(ActionEvent evt) {
                try {
                    if (undo.canRedo()) {
                        undo.redo();
                    }
                } catch (CannotRedoException e) {
                    // Ignore
                }
            }
        });
        
        // Bind the redo action to CTRL-Y
        editor.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
        
        /*
         * Add the JPopup component to the editor
         */
        
        // initialize the JPopup menu items
        jMenuCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        jMenuCut.addActionListener(evt -> editor.cut());
        
        jMenuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        jMenuCopy.addActionListener(evt -> editor.copy());
        
        jMenuPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
        jMenuPaste.addActionListener(evt -> editor.paste());
        
        jMenuSelectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        jMenuSelectAll.addActionListener(evt -> editor.selectAll());
        
        jMenuCopyAll.addActionListener(evt -> {
            editor.selectAll();
            editor.copy();
        });
        
        // add the mouse listener to the editor
        editor.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    getPopupMenu(editor).show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        
        setLayout(new BorderLayout(0, 0));
        add(editor, BorderLayout.CENTER);
    }
    
    /**
     * Retrieve the Edit JPopup
     * 
     * @param area The editor
     * @return The pop-up menu
     */
    protected JPopupMenu getPopupMenu(JTextField area) {
        area.grabFocus();
        
        if (popupMenu == null) {
            popupMenu = new JPopupMenu();
            popupMenu.add(jMenuCut);
            popupMenu.add(jMenuCopy);
            popupMenu.add(jMenuPaste);
            popupMenu.addSeparator();
            popupMenu.add(jMenuSelectAll);
            popupMenu.add(jMenuCopyAll);
        }
        return popupMenu;
    }
    
    /**
     * Get the text from the editor
     */
    public String getText() {
        return editor.getText();
    }
    
    /**
     * Set a text to the editor
     * 
     * @param text The text to be set on the editor
     */
    public void setText(String text) {
        editor.setText(text);
    }
    
    /**
     * Copy all text from the editor
     */
    public void copyAllText() {
        editor.selectAll();
        editor.copy();
    }
    
}
