
package videotools.gui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnhancedGrid extends javax.swing.JPanel {
    
    private static Logger logger = LoggerFactory.getLogger(EnhancedGrid.class);
    
    private static final long serialVersionUID = 1L;
    private EnhancedTableModel model = new EnhancedTableModel();
    private JComboBoxCellEditor comboEditor = new JComboBoxCellEditor();
    private transient TableCellRenderer cellRenderer = new EnhancedCellRenderer();
    private List<Integer> colAlignment = new ArrayList<>();
    private boolean editable;
    private JTable jTable;
    
    class EnhancedTableModel extends DefaultTableModel {
        private static final long serialVersionUID = 1L;
        private Map<String, Class<?>> columnClassMap = new HashMap<>();
        
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Class<?> c = columnClassMap.get(getColumnName(columnIndex));
            return (c == null ? Object.class : c);
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
            return isValidCol(column) && isEditable(model.getColumnName(column), row);
        }
        
        public void setColumClass(String colName, Class<?> colClass) {
            columnClassMap.put(colName, colClass);
        }
    }
    
    /**
     * The default cell renderer, overridden to show CheckBoxes and images
     */
    class EnhancedCellRenderer extends DefaultTableCellRenderer {
        
        private static final long serialVersionUID = 1L;
        
        @Override
        public Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {
            
            JComponent cmp = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                    column);
            Color backGround = cmp.getBackground();
            
            if (value != null) {
                
                String line = value.toString();
                
                if (value instanceof Boolean) {
                    // If it's a boolean value, return the JCheckBox
                    JCheckBox checkBox = new JCheckBox();
                    checkBox.setSelected(((Boolean) value).booleanValue());
                    checkBox.setHorizontalAlignment(colAlignment.get(column).intValue());
                    
                    cmp = checkBox;
                } else if (value instanceof DefaultComboBoxModel) {
                    cmp = new JComboBox<>((DefaultComboBoxModel<?>) value);
                } else if (value instanceof ImageIcon) {
                    JLabel label = new JLabel((ImageIcon) value);
                    
                    label.setHorizontalAlignment(colAlignment.get(column).intValue());
                    cmp = label;
                } else if (line.indexOf('\n') > -1) {
                    // Multiline texts must be added to a text area
                    cmp = new JTextArea(line);
                }
            }
            cmp.setBackground(backGround);
            cmp.setOpaque(isSelected);
            
            return cmp;
        }
    }
    
    /**
     * JComboBoxCellEditor - Class responsible to allow the editing of one JComboBoxCell
     * 
     * @author oscar
     */
    public class JComboBoxCellEditor extends DefaultCellEditor implements ItemListener {
        
        private static final long serialVersionUID = 1L;
        
        public JComboBoxCellEditor() {
            super(new JComboBox<>());
        }
        
        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
                int column) {
            
            return new JComboBox<>((DefaultComboBoxModel<?>) value);
        }
        
        @Override
        public Object getCellEditorValue() {
            return ((JComboBox<?>) getComponent()).getSelectedItem();
        }
        
        public void itemStateChanged(ItemEvent e) {
            super.fireEditingStopped();
        }
    }
    
    /** Creates new customizer EnhancedJTable */
    public EnhancedGrid() {
        
        jTable = new JTable();
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable.setColumnSelectionAllowed(false);
        jTable.setRowSelectionAllowed(true);
        jTable.getTableHeader().setReorderingAllowed(false);
        jTable.setModel(model);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setSorteable(true);
        setEditable(false);
        setLayout(new GridLayout(1, 1));
        
        add(new JScrollPane(jTable));
        
    }
    
    /**
     * @param sort True to make the table sort
     */
    public void setSorteable(boolean sort) {
        jTable.setRowSorter(sort ? new TableRowSorter<>(model) : null);
    }
    
    /**
     * @return True if the table can be sorted
     */
    public boolean isSorteable() {
        return jTable.getRowSorter() != null;
    }
    
    /**
     * Set the table editable mode
     * 
     * @param editable The editable mode
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    
    /**
     * Return if a cell can be edited
     */
    public boolean isEditable(String colName, int row) {
        return editable || (getCellValue(colName, row) instanceof DefaultComboBoxModel);
    }
    
    /**
     * Add a new column to the grid
     * 
     * @param colName The column name
     * @param width The column preferred width
     * @param columClass The column cell class
     * @param alignment One of SwingConstants.LEFT, SwingConstants.RIGHT or SwingConstants.CENTER
     */
    public void addCol(String colName, int width, Class<?> columClass, Integer alignment) {
        if (model.findColumn(colName) < 0) {
            model.addColumn(colName);
            model.setColumClass(colName, columClass);
            for (int i = 0; i < +model.getColumnCount(); i++) {
                getColumnModel().getColumn(i).setCellRenderer(cellRenderer);
            }
            colAlignment.add(alignment);
            TableColumn column = getColumnModel().getColumn(model.findColumn(colName));
            
            column.setPreferredWidth(width);
            column.setResizable(true);
            column.sizeWidthToFit();
        }
    }
    
    /**
     * Set a field value by the column name
     * 
     * @param value The new value
     * @param colName The column name
     * @param row The row
     */
    public void setCellValue(String colName, int row, Object value) {
        int col = -1;
        try {
            col = model.findColumn(colName);
            model.setValueAt(value, jTable.convertRowIndexToModel(row), col);
            
            if (value instanceof DefaultComboBoxModel) {
                getColumnModel().getColumn(col).setCellEditor(comboEditor);
            }
            
        } catch (Exception e) {
            // This message is intended to be displayed only when in debug mode
            logger.error(String.format("Could not select the line (%d,%d) .", row, col));
            e.printStackTrace();
        }
    }
    
    /**
     * Get the value of a cell by using the column name
     * 
     * @param col The column index
     * @param row The row
     * @return The cell value
     */
    public Object getCellValue(String colName, int row) {
        int col = model.findColumn(colName);
        return isValidCol(col) && isValidRow(row) ? model.getValueAt(jTable.convertRowIndexToModel(row), col) : null;
    }
    
    /**
     * Set a field value by the column name
     * 
     * @param value The new value
     * @param colName The column name
     */
    public void setSelectedRowCellValue(String colName, Object value) {
        setCellValue(colName, getSelectedRow(), value);
    }
    
    /**
     * Retrieve the list of all columns
     * 
     * @return The list of all columns
     */
    public List<String> getColNames() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < model.getColumnCount(); i++) {
            list.add(model.getColumnName(i));
        }
        return list;
    }
    
    /**
     * Get the selected column name
     * 
     * @return The selected column name
     */
    public String getSelectedCol() {
        int col = jTable.getSelectedColumn();
        return isValidCol(col) ? model.getColumnName(col) : null;
    }
    
    /**
     * Get the number of columns
     * 
     * @return The number of columns
     */
    public int getColCount() {
        return model.getColumnCount();
    }
    
    /**
     * Get the number of rows from the table
     * 
     * @return The number of rows
     */
    public int getRowCount() {
        return model.getRowCount();
    }
    
    /**
     * Set the number of rows on the table
     * 
     * @param rows The number of rows
     */
    public void setRowCount(int rows) {
        model.setRowCount(rows);
    }
    
    /**
     * Sets the height, in pixels, of all cells to <code>rowHeight</code>, revalidates, and repaints. The height of the cells will be equal to the row height
     * minus the row margin.
     * 
     * @param rowHeight new row height
     */
    public void setRowHeight(int rowHeight) {
        jTable.setRowHeight(rowHeight);
    }
    
    /**
     * Set the preferred value to a column height
     * 
     * @param row The row number
     * @param height The fixed size
     */
    public void setRowHeight(int row, int height) {
        if (isValidRow(row)) {
            jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            jTable.setRowHeight(row, height);
        }
    }
    
    /**
     * Set a fixed value to a column width
     * 
     * @param width The column size
     * @param colName The column name
     */
    public void setColWidth(String colName, int width) {
        int col = model.findColumn(colName);
        if (isValidCol(col)) {
            getColumnModel().getColumn(col).setPreferredWidth(width);
            getColumnModel().getColumn(col).setMinWidth(width);
            getColumnModel().getColumn(col).setMaxWidth(width);
        }
    }
    
    /**
     * Set the table selection mode The selection mode used by the row and column selection models: ListSelectionModel.SINGLE_SELECTION
     * ListSelectionModel.SINGLE_INTERVAL_SELECTION MULTIPLE_INTERVAL_SELECTION ListSelectionModel.MULTIPLE_INTERVAL_SELECTION
     * 
     * @param selectionMode The selection mode
     */
    public void setSelectionMode(int selectionMode) {
        jTable.setSelectionMode(selectionMode);
    }
    
    /**
     * Select a range of rows
     * 
     * @param rows The range of rows
     */
    public void setSelectedRows(int[] rows) {
        jTable.getSelectionModel().clearSelection();
        Arrays.stream(rows).filter(row -> isValidRow(row))
                .forEach(row -> jTable.getSelectionModel().addSelectionInterval(row, row));
    }
    
    /**
     * Get the selected real rows
     * 
     * @return The selected rows
     */
    public int[] getSelectedRows() {
        return jTable.getSelectedRows();
    }
    
    /**
     * Get the selected row index
     * 
     * @return The row index
     */
    public int getSelectedRow() {
        return jTable.getSelectedRow();
    }
    
    /**
     * Get the number of selected rows
     * 
     * @return The number of selected rows
     */
    public int getSelectedRowCount() {
        return jTable.getSelectedRowCount();
    }
    
    /**
     * Set the active row
     * 
     * @param newRow The new row
     * @param scroll True to scroll
     */
    public void setSelectedRow(final int row, boolean scroll) {
        if (isValidRow(row)) {
            jTable.setRowSelectionInterval(row, row);
        } else {
            // This message is intended to be displayed only when in debug mode
            logger.error("Could not select the line {}", row);
        }
        if (scroll) {
            // Set the row visible
            SwingUtilities.invokeLater(
                    () -> scrollRectToVisible(jTable.getCellRect(jTable.convertRowIndexToModel(row), 0, false)));
        }
    }
    
    /**
     * Select the next row
     */
    public void selectNextRow() {
        setSelectedRow(Math.min(getSelectedRow() + 1, getRowCount() - 1), true);
    }
    
    /**
     * Select the previous row
     */
    public void selectPreviousRow() {
        setSelectedRow(Math.max(getSelectedRow() - 1, 0), true);
    }
    
    /**
     * Unselect all rows
     */
    public void selectNone() {
        for (int i = 0; i < getRowCount(); i++) {
            jTable.setRowSelectionInterval(0, 0);
        }
    }
    
    /**
     * Select all rows
     */
    public void selectAll() {
        jTable.selectAll();
    }
    
    /**
     * Move the selected row one direction. Valid directions are: SwingConstants.TOP SwingConstants.BOTTOM SwingConstants.NEXT SwingConstants.PREVIOUS
     * 
     * @param direction the move direction,
     */
    public void moveSelectedRows(int direction) {
        int[] rows = getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            rows[i] = jTable.convertRowIndexToModel(i);
        }
        jTable.getSelectionModel().clearSelection();
        
        int[] newSelection = new int[rows.length];
        for (int i = 0; i < rows.length; i++) {
            int row = rows[i];
            switch (direction) {
                case SwingConstants.BOTTOM:
                    newSelection[i] = getRowCount() - 1;
                    break;
                case SwingConstants.TOP:
                    newSelection[i] = 0;
                    break;
                case SwingConstants.NEXT:
                    newSelection[i] = Math.min(row + 1, getRowCount());
                    break;
                case SwingConstants.PREVIOUS:
                    newSelection[i] = Math.max(0, row - 1);
                    break;
                default:
                    throw new IllegalArgumentException(
                            "Valid directions are:  SwingConstants.TOP, SwingConstants.BOTTOM, SwingConstants.NEXT and SwingConstants.PREVIOUS");
            }
            model.moveRow(row, row, newSelection[i]);
        }
        setSelectedRows(newSelection);
    }
    
    /**
     * Remove the selected row
     */
    public void removeSelectedRow() {
        removeRow(getSelectedRow());
    }
    
    /**
     * Remove a row
     * 
     * @param row The row to be removed
     */
    public void removeRow(int row) {
        if (isValidRow(row)) {
            model.removeRow(jTable.convertRowIndexToModel(row));
        }
    }
    
    private boolean isValidRow(int row) {
        return row > -1 && row < getRowCount();
    }
    
    private boolean isValidCol(int col) {
        return col > -1 && col < getColCount();
    }
    
    /**
     * Allow the selection of multiple rows
     */
    public void allowMultipleRows() {
        jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jTable.setColumnSelectionAllowed(false);
        jTable.setRowSelectionAllowed(true);
    }
    
    /**
     * Returns the <code>TableColumnModel</code> that contains all column information of this table.
     * 
     * @return the object that provides the column state of the table
     * @see #setColumnModel
     */
    public TableColumnModel getColumnModel() {
        return jTable.getColumnModel();
    }
    
    @Override
    public synchronized void addMouseListener(MouseListener l) {
        jTable.addMouseListener(l);
    }
    
}
