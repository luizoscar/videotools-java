/****************************************************************************
 * LogViewerDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import videotools.gui.components.EnhacedTextArea;
import videotools.utils.Constants;
import videotools.utils.Utils;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class LogViewerDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private static Logger logger = LoggerFactory.getLogger(LogViewerDialog.class);
    
    public LogViewerDialog(JFrame parent) {
        super(parent, "Log viewer");
        setLocation(parent.getLocation());
        setSize(parent.getSize());
        
        JPanel paCenter = new JPanel(new BorderLayout());
        paCenter.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        EnhacedTextArea textArea = new EnhacedTextArea();
        textArea.setEnabled(false);
        
        Path logFile = Paths.get(Constants.LOG_FILE_NAME).toAbsolutePath();
        if (Files.exists(logFile)) {
            try {
                textArea.appendLines(Files.readAllLines(logFile));
            } catch (IOException e) {
                logger.error(String.format("Failed to load the log file: %s ", e.getMessage()), e);
                e.printStackTrace();
            }
        }
        
        paCenter.add(textArea, BorderLayout.CENTER);
        
        JPanel panel = new JPanel(new FlowLayout());
        JButton btClose = Utils.createButton("Close", Constants.ICON_EXIT);
        btClose.addActionListener(evt -> dispose());
        panel.add(btClose);
        
        paCenter.add(panel, BorderLayout.SOUTH);
        
        setContentPane(paCenter);
    }
}
