/****************************************************************************
 * VideoConcatenateDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.dialogs;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import videotools.gui.components.EnhancedProgressDialog;
import videotools.utils.Utils;
import videotools.utils.settings.ProgramSettings;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class VideoConcatenateDialog extends EnhancedProgressDialog {
    private static final long serialVersionUID = 1L;
    private String outputFileName;
    
    public VideoConcatenateDialog(JFrame parent, String outputFileName, List<VideoInfo> videosToProcess,
            CodecInfo codec) {
        super(parent, "Concatenating videos", false, videosToProcess, codec);
        this.outputFileName = outputFileName;
    }
    
    @Override
    public void run() {
        try {
            
            long ini = System.currentTimeMillis();
            StringBuilder bld = new StringBuilder();
            int totalSecs = 0;
            int totalBytes = 0;
            for (VideoInfo videoInfo : getVideosToProcess()) {
                bld.append("file \'").append(videoInfo.getPath().toAbsolutePath().toString()).append("\'\n");
                totalSecs += videoInfo.getNumberOfSeconds();
                totalBytes += videoInfo.getBytes();
            }
            
            String msg = String.format("Concatenating %d files - %s (%s)", getVideosToProcess().size(),
                    Utils.getTimeFromSeconds(totalSecs), Utils.getFormattedByteCount(totalBytes));
            logger.info(msg);
            
            // Create the temp file
            Path tempFile = Files.createTempFile("tempfiles", ".tmp");
            Files.write(tempFile, Arrays.asList(bld.toString().split("\n")), Charset.defaultCharset(),
                    StandardOpenOption.WRITE);
            
            setInputVideoSecs(totalSecs);
            setOutputVideoBytes(0);
            setOutputVideoSecs(0);
            
            Path out = Paths.get(outputFileName);
            setOutputVideoFile(out);
            
            // Override existing file
            if (Files.isRegularFile(Paths.get(outputFileName))) {
                logger.info("Overriding existing file: {} ", Paths.get(outputFileName).toAbsolutePath());
                Files.delete(Paths.get(outputFileName).toAbsolutePath());
            }
            
            logger.info("------------------------------------------------------------------");
            logger.info(msg);
            setJobDescription(msg);
            
            // ffmpeg parameter. Note: Order matters
            List<String> parmList = new ArrayList<>();
            parmList.addAll(Arrays.asList("-y", "-hide_banner", "-strict", "-2"));
            parmList.addAll(Arrays.asList("-f", "concat", "-safe", "0"));
            parmList.addAll(getOuputCodec().getInputParams());
            parmList.addAll(Arrays.asList("-i", tempFile.toAbsolutePath().toString()));
            parmList.addAll(getOuputCodec().getOutputParams());
            parmList.add(getOutputVideoFile().toAbsolutePath().toString());
            
            // Run ffmpeg
            executeProcess(ProgramSettings.getInstance().getPathToFfmpeg(), 1, parmList.toArray(new String[] {}));
            
            Files.delete(tempFile);
            logger.info("------------------------------------------------------------------");
            logger.info("Processed {} videos in {} sec", getVideosToProcess().size(), Utils.getNumberOfSeconds(ini));
        } catch (IOException e) {
            logger.error(String.format("Failed to concatenate the files: %s", e.getMessage()), e);
        }
        dispose();
    }
    
}
