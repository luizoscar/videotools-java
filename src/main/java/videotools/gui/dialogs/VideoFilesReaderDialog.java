/****************************************************************************
 * VideoFilesReaderDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.dialogs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;

import org.apache.commons.lang3.StringUtils;

import videotools.gui.components.EnhancedProgressDialog;
import videotools.utils.Constants;
import videotools.utils.Utils;
import videotools.utils.settings.ProgramSettings;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class VideoFilesReaderDialog extends EnhancedProgressDialog {
    private static final long serialVersionUID = 1L;
    
    // Pattern to capture all video details from ffmpeg output
    private static final Pattern pattern = Pattern.compile(
            Constants.REGEX_DURATION + '|' + Constants.REGEX_VIDEO + '|' + Constants.REGEX_AUDIO,
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    
    private transient List<VideoInfo> parsedVideos;
    private transient Map<Path, VideoInfo> existingVideos;
    
    public VideoFilesReaderDialog(JFrame parent, Map<Path, VideoInfo> existingVideos, CodecInfo codec) {
        super(parent, "Searching for video files", false, null, codec);
        parsedVideos = new ArrayList<>();
        this.existingVideos = existingVideos;
    }
    
    /**
     * @return The current value from the field parsedVideos
     */
    public List<VideoInfo> getParsedVideos() {
        return parsedVideos;
    }
    
    /**
     * Capture and parse the information from ffmpeg
     * 
     * @param filePath The video file
     * @return The parsed video information
     */
    private VideoInfo extractVideoInfo(Path filePath) {
        VideoInfo resp = existingVideos.containsKey(filePath) ? existingVideos.get(filePath) : new VideoInfo();
        if (!wasCanceled()) {
            try {
                // Prevent fetching information for videos already on cache
                FileTime fileTime = Files.getLastModifiedTime(filePath);
                if (resp.getLastModified() != null && fileTime.compareTo(resp.getLastModified()) <= 0) {
                    logger.info("Using video information from cache: {}", filePath);
                    return resp;
                }
                
                // Run ffmpeg and capture valid lines
                StringBuilder bld = new StringBuilder();
                executeProcess(ProgramSettings.getInstance().getPathToFfmpeg(), 1, "-hide_banner", "-i",
                        filePath.toAbsolutePath().toString()).stream()
                                .filter(l -> StringUtils.containsAny(l, "Stream #0", " Duration:"))
                                .forEach(bld::append);
                
                // Extract the video details
                Matcher m = pattern.matcher(bld);
                StringBuilder bld2 = new StringBuilder();
                while (m.find()) {
                    bld2.append(m.group()).append(' ');
                }
                resp.setDetails(bld2.toString());
                resp.setBytes(Files.size(filePath));
                resp.setLastModified(fileTime);
                resp.setPath(filePath);
                resp.setLastModified(fileTime);
                
            } catch (IOException e) {
                logger.error(String.format("Failed to retrieve the details of the video file: %s", e.getMessage()), e);
            }
        }
        return resp;
    }
    
    /**
     * Locate the video files no the directory
     * 
     * @param parentDir The parent dir
     * @return The list of video file Path
     */
    private List<Path> locateValidFiles(final String parentDir) {
        setJobDescription("Searching video files from: " + parentDir);
        setCurrentIndeterminate(true);
        
        List<Path> resp = new ArrayList<>();
        try (Stream<Path> entries = Files.walk(Paths.get(parentDir))) {
            resp = entries.filter(f -> isVideo(f, ProgramSettings.getInstance().getVideoExtensions()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            logger.error(String.format("Failed to build the list of files: %s", e.getMessage()), e);
        }
        
        setCurrentIndeterminate(false);
        return resp;
    }
    
    /**
     * Note: Since the operation is to locate files, we disable the loop over the selected files and implement the file search logic here
     */
    @Override
    public void run() {
        long ini = System.currentTimeMillis();
        // Search the video files
        List<Path> paths = locateValidFiles(ProgramSettings.getInstance().getVideosPath());
        
        // Parse each video information using ffmpeg
        for (int i = 0; i < paths.size(); i++) {
            Path p = paths.get(i);
            logger.info("------------------------------------------------------------------");
            logger.info("Checking file [{} / {}] - {}", i + 1, paths.size(), p.getFileName());
            
            setCurrentProgress(0, paths.size(), i);
            setCurrentStatus(String.format("Checking file [%d / %d] - %s", i + 1, paths.size(), p.getFileName()));
            parsedVideos.add(extractVideoInfo(p));
        }
        logger.info("------------------------------------------------------------------");
        
        logger.info("Verified {} videos in {} sec", paths.size(), Utils.getNumberOfSeconds(ini));
        dispose();
    }
    
}
