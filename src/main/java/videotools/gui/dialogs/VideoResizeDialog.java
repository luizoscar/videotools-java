/****************************************************************************
 * VideoResizeDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.dialogs;

import java.awt.Dimension;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import javax.swing.JFrame;

import videotools.gui.components.EnhancedProgressDialog;
import videotools.utils.Constants;
import videotools.utils.Utils;
import videotools.utils.settings.ProgramSettings;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class VideoResizeDialog extends EnhancedProgressDialog {
    private static final long serialVersionUID = 1L;
    private Dimension res;
    
    public VideoResizeDialog(JFrame parent, List<VideoInfo> videosToConvert, Dimension res, CodecInfo codec) {
        super(parent, String.format("Resizing videos: %dX%d", res.width, res.height), true, videosToConvert, codec);
        this.res = res;
    }
    
    @Override
    protected void processVideo(VideoInfo info) {
        Path originalVideo = info.getPath();
        try {
            setInputVideoSecs(info.getNumberOfSeconds());
            setOutputVideoBytes(0);
            setOutputVideoSecs(0);
            
            buildOutputVideoFile(originalVideo, "_resized" + getOuputCodec().getFileSuffix());
            Matcher matcher = Constants.PATTERN_RESOLUTION.matcher(info.getFormattedVideoCodec());
            String oldRes = "Unknow res.";
            if (matcher.find()) {
                oldRes = matcher.group(0);
            }
            String msg = String.format("From: %s - %s (%s) [%s]", originalVideo.getFileName(),
                    Utils.getTimeFromSeconds(getInputVideoSecs()),
                    Utils.getFormattedByteCount(Files.size(originalVideo)), oldRes);
            setGlobalStatus(msg);
            logger.info(msg);
            
            // ffmpeg parameter. Note: Order matters
            List<String> parmList = new ArrayList<>();
            parmList.addAll(Arrays.asList("-y", "-hide_banner", "-strict", "-2"));
            parmList.addAll(getOuputCodec().getInputParams());
            parmList.addAll(Arrays.asList("-i", originalVideo.toAbsolutePath().toString()));
            parmList.addAll(
                    Arrays.asList("-vf", "scale=w=" + res.width + ":h=" + res.height, "-q:a", "0", "-q:v", "0"));
            parmList.addAll(getOuputCodec().getOutputParams());
            parmList.add(getOutputVideoFile().toAbsolutePath().toString());
            
            // Run ffmpeg
            executeProcess(ProgramSettings.getInstance().getPathToFfmpeg(), 1, parmList.toArray(new String[] {}));
            
            setProcessedSecs(getProcessedSecs() + getInputVideoSecs());
        } catch (IOException e) {
            logger.error(String.format("Failed to convert the file %s: %s", originalVideo.toAbsolutePath().toString(),
                    e.getMessage()), e);
        }
    }
    
}
