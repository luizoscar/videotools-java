/****************************************************************************
 * VideoEditSequenceDialog.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui.dialogs;

import java.awt.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.*;

import org.apache.commons.lang3.StringUtils;

import videotools.gui.components.EnhacedTextArea;
import videotools.gui.components.EnhacedTextField;
import videotools.utils.Constants;
import videotools.utils.Utils;
import videotools.utils.settings.CodecSettings;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class VideoEditSequenceDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    
    private JComboBox<CodecInfo> cbCodecs;
    private EnhacedTextField textOutput;
    private EnhacedTextArea textArea;
    private transient List<VideoInfo> videosToConcatenate = Collections.emptyList();
    private transient List<VideoInfo> videosToConvert;
    
    public VideoEditSequenceDialog(JFrame parent, List<VideoInfo> videosToConvert, String sourceDir) {
        super(parent, "Choose the video sequence to concatenate", true);
        setSize(600, 480);
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = this.getSize();
        Point p = new Point((dim.width - windowSize.width) / 2, (dim.height - windowSize.height) / 2);
        setLocation(p);
        
        JPanel paTop = new JPanel(new BorderLayout());
        paTop.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        textArea = new EnhacedTextArea();
        
        this.videosToConvert = videosToConvert;
        for (VideoInfo videoInfo : videosToConvert) {
            textArea.append(videoInfo.getPath().toAbsolutePath().toString());
            textArea.append("\n");
        }
        paTop.add(textArea, BorderLayout.CENTER);
        
        JPanel paOptions = new JPanel(new GridBagLayout());
        paOptions.add(new JLabel("Output file codec:"),
                Utils.buildConstraints(GridBagConstraints.FIRST_LINE_START, 0, 0));
        cbCodecs = new JComboBox<>();
        for (CodecInfo codec : CodecSettings.getInstance().getVideoCodecs()) {
            cbCodecs.addItem(codec);
        }
        GridBagConstraints c1 = Utils.buildConstraints(GridBagConstraints.LINE_START, 1, 0);
        c1.fill = GridBagConstraints.NONE;
        paOptions.add(cbCodecs, c1);
        
        Path outFile = Paths.get(sourceDir).resolve("concatenated.mp4").toAbsolutePath();
        paOptions.add(new JLabel("Output file name:"), Utils.buildConstraints(GridBagConstraints.LINE_START, 0, 1));
        textOutput = new EnhacedTextField(outFile.toString());
        paOptions.add(textOutput, Utils.buildConstraints(GridBagConstraints.LAST_LINE_END, 1, 1));
        paTop.add(paOptions, BorderLayout.SOUTH);
        
        add(paTop, BorderLayout.CENTER);
        
        JPanel panel = new JPanel(new FlowLayout());
        JButton btOk = Utils.createButton("OK", Constants.ICON_OK);
        btOk.addActionListener(evt -> continueConcatenating());
        panel.add(btOk);
        
        JButton btClose = Utils.createButton("Cancel", Constants.ICON_EXIT);
        btClose.addActionListener(evt -> setVisible(false));
        panel.add(btClose);
        
        add(panel, BorderLayout.SOUTH);
    }
    
    private void continueConcatenating() {
        videosToConcatenate = new ArrayList<>();
        
        for (String line : StringUtils.split(textArea.getText(), "\n")) {
            if (!StringUtils.isBlank(line)) {
                for (VideoInfo info : videosToConvert) {
                    if (StringUtils.equalsAnyIgnoreCase(line, info.getPath().toAbsolutePath().toString())) {
                        videosToConcatenate.add(info);
                        break;
                    }
                }
            }
        }
        
        setVisible(false);
    }
    
    /**
     * @return The current value from the field videoSequence
     */
    public List<VideoInfo> getVideosToConcatenate() {
        return videosToConcatenate;
    }
    
    /**
     * @return the name of the output file
     */
    public String getOutputFile() {
        return textOutput.getText();
    }
    
    public CodecInfo getSelectedCodec() {
        return (CodecInfo) cbCodecs.getSelectedItem();
    }
}
