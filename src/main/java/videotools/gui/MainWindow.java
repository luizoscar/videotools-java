/****************************************************************************
 * MainWindow.java
 *
 * Copyright (c) 2020 - Luiz Oscar Machado Barbosa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/

package videotools.gui;

import static java.awt.GridBagConstraints.*;
import static javax.swing.JOptionPane.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import javax.swing.*;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import videotools.gui.components.EnhacedTextField;
import videotools.gui.components.EnhancedGrid;
import videotools.gui.dialogs.*;
import videotools.utils.Constants;
import videotools.utils.Utils;
import videotools.utils.VideoRotation;
import videotools.utils.settings.CodecSettings;
import videotools.utils.settings.ProgramSettings;
import videotools.utils.to.CodecInfo;
import videotools.utils.to.VideoInfo;

/**
 * @author Luiz Oscar Machado Barbosa <luizoscar@gmail.com>
 */
public class MainWindow extends JFrame {
    private static final String NEW_VIDEO_FORMAT = "New video format:";
    private static final long serialVersionUID = 1L;
    private static Logger logger = LoggerFactory.getLogger(MainWindow.class);
    
    JButton buttonRefresh;
    JButton buttonConvert;
    JButton buttonResize;
    JButton buttonRotate;
    JButton buttonInterval;
    JButton buttonCrop;
    JButton buttonConcatenate;
    JButton buttonLogs;
    JButton buttonExit;
    
    JLabel labelStatus;
    EnhancedGrid gridFiles;
    EnhacedTextField textFieldSourceDir;
    String videoExtensions;
    String pathToFfmpeg = SystemUtils.IS_OS_WINDOWS ? "ffmpeg.exe" : "ffmpeg_ubuntu.sh";
    
    public MainWindow() {
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setSize(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT);
        this.setTitle(Constants.TITLE);
        setResizable(true);
        createMainPanel();
    }
    
    private void loadSettings() {
        // get screen size
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = this.getSize();
        // window position
        Point p = new Point((dim.width - windowSize.width) / 2, (dim.height - windowSize.height) / 2);
        
        // Set the default positions and load the settings
        ProgramSettings settings = ProgramSettings.getInstance();
        settings.setWindowPosition(p);
        settings.setWindowSize(windowSize);
        settings.loadSettings();
        
        // set the position
        this.setLocation(settings.getWindowPosition());
        this.setSize(settings.getWindowSize());
        this.textFieldSourceDir.setText(settings.getVideosPath());
        
        // Load the codecs
        CodecSettings.getInstance().loadSettings();
    }
    
    private JPanel createCenterPanel() {
        JPanel panelCenter = new JPanel(new BorderLayout());
        
        // Default spaces between components
        Insets insets = new Insets(5, 5, 5, 5);
        
        // TOP Section
        
        // Source dir top panel
        JPanel panelTop = new JPanel(new GridBagLayout());
        
        // Source Label
        JLabel label = new JLabel("Videos directory:", SwingConstants.LEFT);
        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 0;
        c1.anchor = LINE_START;
        c1.insets = insets;
        panelTop.add(label, c1);
        
        // Source editor
        textFieldSourceDir = new EnhacedTextField();
        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 1;
        c2.fill = HORIZONTAL;
        c2.anchor = CENTER;
        c2.weightx = 1;
        c2.insets = insets;
        panelTop.add(textFieldSourceDir, c2);
        
        // Source button
        JButton buttonOpenDir = Utils.createButton("Open", Constants.ICON_OPEN);
        buttonOpenDir.addActionListener(l -> selectSourceDir());
        
        GridBagConstraints c3 = new GridBagConstraints();
        c3.gridx = 2;
        c3.anchor = LINE_END;
        c3.insets = insets;
        panelTop.add(buttonOpenDir, c3);
        
        panelCenter.add(panelTop, BorderLayout.NORTH);
        
        // BOTTOM section
        
        // Status bottom panel
        JPanel panelBottom = new JPanel(new GridBagLayout());
        labelStatus = new JLabel("", SwingConstants.LEFT);
        
        GridBagConstraints c4 = new GridBagConstraints();
        c4.fill = HORIZONTAL;
        c4.anchor = LINE_START;
        c4.weightx = 1;
        c4.insets = insets;
        panelBottom.add(labelStatus, c4);
        
        panelCenter.add(panelBottom, BorderLayout.SOUTH);
        
        // CENTER section
        gridFiles = new EnhancedGrid();
        gridFiles.addCol(Constants.COL_NAME_SELECTED, 20, Boolean.class, SwingConstants.CENTER);
        gridFiles.addCol(Constants.COL_NAME_FILE, 150, String.class, SwingConstants.LEFT);
        gridFiles.addCol(Constants.COL_NAME_SIZE, 40, String.class, SwingConstants.LEFT);
        gridFiles.addCol(Constants.COL_NAME_DURATION, 30, String.class, SwingConstants.LEFT);
        gridFiles.addCol(Constants.COL_NAME_VIDEO, 30, String.class, SwingConstants.LEFT);
        gridFiles.addCol(Constants.COL_NAME_AUDIO, 70, String.class, SwingConstants.LEFT);
        
        gridFiles.setColWidth(Constants.COL_NAME_SELECTED, 50);
        gridFiles.setColWidth(Constants.COL_NAME_SIZE, 80);
        
        gridFiles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    handleGridLeftClick(e);
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    handleGridRightClick(e);
                }
            }
        });
        
        panelCenter.add(gridFiles, BorderLayout.CENTER);
        
        return panelCenter;
    }
    
    /**
     * Right mouse click
     * 
     * @param e
     */
    private void handleGridRightClick(MouseEvent e) {
        JPopupMenu menu = new JPopupMenu();
        menu.add(Utils.createMenuItem("Check all files", l -> setSelectionForAll(true)));
        menu.add(Utils.createMenuItem("Uncheck all files", l -> setSelectionForAll(false)));
        menu.addSeparator();
        menu.add(Utils.createMenuItem("Delete the selected files", l -> {
            if (Utils.askQuestion(MainWindow.this,
                    "Do you really want to remove the selected files?\nNote: this operation can not be undone.")) {
                
                getSelectedVideos().stream().forEach(info -> {
                    try {
                        Files.delete(info.getPath());
                    } catch (IOException ex) {
                        logger.error(String.format("Failed to remove the file %s: %s", info.getPath(), ex.getMessage()),
                                ex);
                    }
                });
                updateFileList();
            }
        }));
        menu.addSeparator();
        JMenu mnuSelectByCodec = new JMenu("Check videos from format");
        for (CodecInfo codec : CodecSettings.getInstance().getDecoders()) {
            mnuSelectByCodec.add(Utils.createMenuItem(codec.getName(), l -> {
                for (int i = 0; i < gridFiles.getRowCount(); i++) {
                    VideoInfo info = getVideoInfo(i);
                    if (info != null && StringUtils.contains(info.getFormattedVideoCodec(), codec.getDecoderName())) {
                        setVideoSelection(i, Boolean.TRUE);
                    }
                }
                updateCounters();
            }));
        }
        menu.add(mnuSelectByCodec);
        
        menu.show(gridFiles, e.getX(), e.getY());
        
    }
    
    /**
     * Left mouse click
     * 
     * @param e
     */
    private void handleGridLeftClick(MouseEvent e) {
        if (e.getClickCount() > 1 || Constants.COL_NAME_SELECTED.equals(gridFiles.getSelectedCol())) {
            int row = gridFiles.getSelectedRow();
            if (row > -1) {
                setVideoSelection(row, BooleanUtils.negate(isVideoSelected(row)));
                updateCounters();
            }
        }
    }
    
    /**
     * Select the source dir
     */
    private void selectSourceDir() {
        File dir = Utils.openDirDialog(this, "Select the directory containing the video files",
                textFieldSourceDir.getText());
        if (dir != null && dir.isDirectory()) {
            textFieldSourceDir.setText(dir.getAbsolutePath());
        }
    }
    
    private JPanel createButtonPanel() {
        
        JPanel panelButton = new JPanel(new GridBagLayout());
        
        // Refresh
        buttonRefresh = Utils.createButton("Refresh", Constants.ICON_REFRESH);
        buttonRefresh.addActionListener(l -> updateFileList());
        panelButton.add(buttonRefresh, Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        
        // Blanks
        panelButton.add(new JPanel(), Utils.buildConstraints(CENTER, 0, 1));
        panelButton.add(new JPanel(), Utils.buildConstraints(CENTER, 0, 2));
        
        // Convert
        buttonConvert = Utils.createButton("Convert", Constants.ICON_CONVERT);
        buttonConvert.addActionListener(l -> showConverVideosDialog());
        panelButton.add(buttonConvert, Utils.buildConstraints(CENTER, 0, 3));
        
        // Resize
        buttonResize = Utils.createButton("Resize", Constants.ICON_RESIZE);
        buttonResize.addActionListener(l -> showResizeVideosDialog());
        panelButton.add(buttonResize, Utils.buildConstraints(CENTER, 0, 5));
        
        // Rotate
        buttonRotate = Utils.createButton("Rotate", Constants.ICON_ROTATE);
        buttonRotate.addActionListener(l -> showRotateVideosDialog());
        panelButton.add(buttonRotate, Utils.buildConstraints(CENTER, 0, 6));
        
        // Interval
        buttonInterval = Utils.createButton("Interval", Constants.ICON_INTERVAL);
        buttonInterval.addActionListener(i -> showIntervalVideosDialog());
        panelButton.add(buttonInterval, Utils.buildConstraints(CENTER, 0, 7));
        
        // Crop
        buttonCrop = Utils.createButton("Crop", Constants.ICON_CROP);
        buttonCrop.addActionListener(l -> showCropVideoDialog());
        panelButton.add(buttonCrop, Utils.buildConstraints(CENTER, 0, 8));
        
        // Concatenate
        buttonConcatenate = Utils.createButton("Concatenate", Constants.ICON_CONCATENATE);
        buttonConcatenate.addActionListener(l -> showConcatenateDialog());
        panelButton.add(buttonConcatenate, Utils.buildConstraints(CENTER, 0, 9));
        
        // Blanks
        panelButton.add(new JPanel(), Utils.buildConstraints(CENTER, 0, 10));
        panelButton.add(new JPanel(), Utils.buildConstraints(CENTER, 0, 11));
        
        // Logs
        buttonLogs = Utils.createButton("Logs", Constants.ICON_LOGS);
        panelButton.add(buttonLogs, Utils.buildConstraints(CENTER, 0, 12));
        buttonLogs.addActionListener(l -> {
            LogViewerDialog logView = new LogViewerDialog(this);
            logView.setVisible(true);
        });
        
        // Exit
        buttonExit = Utils.createButton("Exit", Constants.ICON_EXIT);
        panelButton.add(buttonExit, Utils.buildConstraints(LAST_LINE_END, 0, 13));
        buttonExit.addActionListener(l -> dispose());
        
        return panelButton;
    }
    
    private void showConcatenateDialog() {
        VideoEditSequenceDialog seqEdit = new VideoEditSequenceDialog(this, getSelectedVideos(),
                textFieldSourceDir.getText());
        seqEdit.setVisible(true);
        List<VideoInfo> videosToProcess = seqEdit.getVideosToConcatenate();
        String outputFileName = seqEdit.getOutputFile();
        CodecInfo codec = seqEdit.getSelectedCodec();
        seqEdit.dispose();
        if (!CollectionUtils.isEmpty(videosToProcess)) {
            VideoConcatenateDialog dialog = new VideoConcatenateDialog(this, outputFileName, videosToProcess, codec);
            dialog.showAndExecuteTask();
            dialog.dispose();
            updateFileList();
            
        }
    }
    
    private void showCropVideoDialog() {
        VideoInfo selectedVideo = getSelectedVideos().get(0);
        
        String width = "1280";
        String height = "720";
        
        Matcher matcher = Constants.PATTERN_RESOLUTION.matcher(selectedVideo.getFormattedVideoCodec());
        if (matcher.find()) {
            width = matcher.group(1);
            height = matcher.group(2);
        }
        Dimension dim = new Dimension(50, 20);
        JPanel panel = new JPanel(new GridBagLayout());
        
        panel.add(new JLabel("Start X:", SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        EnhacedTextField textX1 = new EnhacedTextField("0");
        textX1.setPreferredSize(dim);
        panel.add(textX1, Utils.buildConstraints(FIRST_LINE_END, 1, 0));
        
        panel.add(new JLabel("Start Y:", SwingConstants.LEFT), Utils.buildConstraints(LINE_START, 2, 0));
        EnhacedTextField textY1 = new EnhacedTextField("0");
        textY1.setPreferredSize(dim);
        panel.add(textY1, Utils.buildConstraints(LINE_END, 3, 0));
        
        panel.add(new JLabel("End X:", SwingConstants.LEFT), Utils.buildConstraints(LINE_START, 0, 1));
        EnhacedTextField textX2 = new EnhacedTextField(width);
        textX2.setPreferredSize(dim);
        panel.add(textX2, Utils.buildConstraints(LINE_END, 1, 1));
        
        panel.add(new JLabel("End Y:", SwingConstants.LEFT), Utils.buildConstraints(LAST_LINE_START, 2, 1));
        EnhacedTextField textY2 = new EnhacedTextField(height);
        textY2.setPreferredSize(dim);
        panel.add(textY2, Utils.buildConstraints(LAST_LINE_END, 3, 1));
        
        GridBagConstraints gbc1 = Utils.buildConstraints(FIRST_LINE_START, 0, 2);
        gbc1.gridwidth = 4;
        panel.add(new JLabel(NEW_VIDEO_FORMAT, SwingConstants.LEFT), gbc1);
        
        JComboBox<CodecInfo> cbCodecs = Utils.buildCodecCombo();
        GridBagConstraints gbc2 = Utils.buildConstraints(FIRST_LINE_END, 0, 3);
        gbc2.gridwidth = 4;
        panel.add(cbCodecs, gbc2);
        
        if (showConfirmDialog(this, panel, "Video area to be captured", OK_CANCEL_OPTION, PLAIN_MESSAGE) == OK_OPTION) {
            try {
                
                int originalW = Utils.getTextNumber(width);
                int originalH = Utils.getTextNumber(height);
                
                Point p1 = new Point(Utils.getTextNumber(textX1.getText()), Utils.getTextNumber(textY1.getText()));
                Point p2 = new Point(Utils.getTextNumber(textX2.getText()), Utils.getTextNumber(textY2.getText()));
                
                Validate.exclusiveBetween(-1, p2.x, p1.x,
                        String.format("The Start X position must be between 0 and %d", p2.x));
                Validate.exclusiveBetween(-1, p2.y, p1.y,
                        String.format("The Start Y position must be between 0 and %d", p2.y));
                
                Validate.exclusiveBetween((p1.x + 1), originalW, p2.x,
                        String.format("The End X position must be between %d and %d", p1.x + 1, originalW));
                Validate.exclusiveBetween((p1.y + 1), originalH, p2.y,
                        String.format("The End Y position must be between %d and %d", p1.y + 1, originalH));
                
                VideoCropDialog dialog = new VideoCropDialog(this, selectedVideo, p1, p2,
                        (CodecInfo) cbCodecs.getSelectedItem());
                dialog.showAndExecuteTask();
                dialog.dispose();
                updateFileList();
                
            } catch (IllegalArgumentException e) {
                Utils.showModalMessageError(this, e.getMessage());
            }
        }
    }
    
    private void showIntervalVideosDialog() {
        VideoInfo selectedVideo = getSelectedVideos().get(0);
        JPanel panel = new JPanel(new GridBagLayout());
        
        panel.add(new JLabel("Video start time:", SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        EnhacedTextField textIni = new EnhacedTextField("00:00:00");
        panel.add(textIni, Utils.buildConstraints(FIRST_LINE_END, 1, 0));
        
        panel.add(new JLabel("Video end time:", SwingConstants.LEFT), Utils.buildConstraints(LAST_LINE_START, 0, 1));
        EnhacedTextField textEnd = new EnhacedTextField(Utils.getTimeFromSeconds(selectedVideo.getNumberOfSeconds()));
        panel.add(textEnd, Utils.buildConstraints(LAST_LINE_END, 1, 1));
        
        GridBagConstraints gbc1 = Utils.buildConstraints(FIRST_LINE_START, 0, 2);
        gbc1.gridwidth = 2;
        panel.add(new JLabel(NEW_VIDEO_FORMAT, SwingConstants.LEFT), gbc1);
        
        JComboBox<CodecInfo> cbCodecs = Utils.buildCodecCombo();
        GridBagConstraints gbc2 = Utils.buildConstraints(FIRST_LINE_END, 0, 3);
        gbc2.gridwidth = 2;
        panel.add(cbCodecs, gbc2);
        
        if (showConfirmDialog(this, panel, "Time section to be captured", OK_CANCEL_OPTION,
                PLAIN_MESSAGE) == OK_OPTION) {
            try {
                Validate.isTrue(Constants.PATTERN_TIME.matcher(textIni.getText()).matches(),
                        "You need to specify the start time in the format hh:mm:ss.");
                Validate.isTrue(Constants.PATTERN_TIME.matcher(textEnd.getText()).matches(),
                        "You need to specify the end time in the format hh:mm:ss.");
                
                int ini = Utils.getNumberOfSeconds(textIni.getText());
                int end = Utils.getNumberOfSeconds(textEnd.getText());
                int maxSecs = selectedVideo.getNumberOfSeconds() + 1;
                String msg = String.format("The start and end time must be between 00:00:00 and %s",
                        Utils.getTimeFromSeconds(selectedVideo.getNumberOfSeconds()));
                
                Validate.exclusiveBetween(-1, maxSecs, ini, msg);
                Validate.exclusiveBetween(-1, maxSecs, end, msg);
                Validate.isTrue(end > ini, "The end time must happen after the start time.");
                
                VideoIntervalDialog dialog = new VideoIntervalDialog(this, selectedVideo, ini, end,
                        (CodecInfo) cbCodecs.getSelectedItem());
                dialog.showAndExecuteTask();
                dialog.dispose();
                updateFileList();
                
            } catch (IllegalArgumentException e) {
                Utils.showModalMessageError(this, e.getMessage());
            }
        }
    }
    
    private void showRotateVideosDialog() {
        JPanel panel = new JPanel(new GridBagLayout());
        
        JComboBox<VideoRotation> cbRotation = new JComboBox<>();
        Stream.of(VideoRotation.values()).forEach(cbRotation::addItem);
        panel.add(new JLabel("Video Rotation:", SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        panel.add(cbRotation, Utils.buildConstraints(FIRST_LINE_END, 0, 1));
        
        panel.add(new JLabel(NEW_VIDEO_FORMAT, SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 2));
        JComboBox<CodecInfo> cbCodecs = Utils.buildCodecCombo();
        panel.add(cbCodecs, Utils.buildConstraints(FIRST_LINE_END, 0, 3));
        
        if (showConfirmDialog(this, panel, "Select the kind of rotation", OK_CANCEL_OPTION,
                PLAIN_MESSAGE) == OK_OPTION) {
            
            VideoRotationDialog dialog = new VideoRotationDialog(this, getSelectedVideos(),
                    (VideoRotation) cbRotation.getSelectedItem(), (CodecInfo) cbCodecs.getSelectedItem());
            dialog.showAndExecuteTask();
            dialog.dispose();
            updateFileList();
        }
    }
    
    private void showResizeVideosDialog() {
        Dimension resolution = new Dimension(1280, 720); // 720p
        
        List<VideoInfo> selectedVideos = getSelectedVideos();
        for (VideoInfo info : selectedVideos) {
            Matcher matcher = Constants.PATTERN_RESOLUTION.matcher(info.getFormattedVideoCodec());
            if (matcher.find()) {
                resolution.setSize(Utils.getTextNumber(matcher.group(1)), Utils.getTextNumber(matcher.group(2)));
                break;
            }
        }
        
        JPanel panel = new JPanel(new GridBagLayout());
        
        panel.add(new JLabel("New Width:", SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        EnhacedTextField textWidth = new EnhacedTextField(Integer.toString((int) resolution.getWidth()));
        panel.add(textWidth, Utils.buildConstraints(FIRST_LINE_END, 1, 0));
        
        panel.add(new JLabel("New Height:", SwingConstants.LEFT), Utils.buildConstraints(LINE_START, 0, 1));
        EnhacedTextField textHeight = new EnhacedTextField(Integer.toString((int) resolution.getHeight()));
        panel.add(textHeight, Utils.buildConstraints(LINE_END, 1, 1));
        
        GridBagConstraints gbc1 = Utils.buildConstraints(LAST_LINE_START, 0, 2);
        gbc1.gridwidth = 2;
        panel.add(new JLabel(NEW_VIDEO_FORMAT, SwingConstants.LEFT), gbc1);
        
        GridBagConstraints gbc2 = Utils.buildConstraints(LAST_LINE_END, 0, 3);
        gbc2.gridwidth = 2;
        JComboBox<CodecInfo> cbCodecs = Utils.buildCodecCombo();
        panel.add(cbCodecs, gbc2);
        
        if (showConfirmDialog(this, panel, "Select the new video resolution", OK_CANCEL_OPTION,
                PLAIN_MESSAGE) == OK_OPTION) {
            
            try {
                Validate.isTrue(Utils.getTextNumber(textWidth.getText()) > 0,
                        "You need to specify the new video Width.");
                Validate.isTrue(Utils.getTextNumber(textHeight.getText()) > 0,
                        "You need to specify the new video Height.");
                
                resolution.setSize(Utils.getTextNumber(textWidth.getText()), Utils.getTextNumber(textHeight.getText()));
                
                VideoResizeDialog dialog = new VideoResizeDialog(this, selectedVideos, resolution,
                        (CodecInfo) cbCodecs.getSelectedItem());
                dialog.showAndExecuteTask();
                dialog.dispose();
                updateFileList();
            } catch (IllegalArgumentException e) {
                Utils.showModalMessageError(this, e.getMessage());
            }
        }
    }
    
    private void showConverVideosDialog() {
        JPanel panel = new JPanel(new GridBagLayout());
        
        panel.add(new JLabel(NEW_VIDEO_FORMAT, SwingConstants.LEFT), Utils.buildConstraints(FIRST_LINE_START, 0, 0));
        
        JComboBox<CodecInfo> cbCodecs = Utils.buildCodecCombo();
        panel.add(cbCodecs, Utils.buildConstraints(FIRST_LINE_END, 0, 1));
        
        if (showConfirmDialog(this, panel, "Select the new video format", OK_CANCEL_OPTION,
                PLAIN_MESSAGE) == OK_OPTION) {
            VideoConverterDialog dialog = new VideoConverterDialog(this, getSelectedVideos(),
                    (CodecInfo) cbCodecs.getSelectedItem());
            dialog.showAndExecuteTask();
            dialog.dispose();
            updateFileList();
        }
    }
    
    private void createMainPanel() {
        JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        mainPanel.add(createButtonPanel(), BorderLayout.EAST);
        mainPanel.add(createCenterPanel(), BorderLayout.CENTER);
        add(mainPanel);
        loadSettings();
        ProgramSettings settings = ProgramSettings.getInstance();
        
        // Save settings at onClose, onMove and onResize
        // Note: Only after loading the application, to prevent overriding the file
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                settings.saveSettings();
            }
        });
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                settings.setWindowPosition(getLocation());
            }
            
            @Override
            public void componentResized(ComponentEvent e) {
                settings.setWindowSize(getSize());
            }
        });
        updateCounters();
    }
    
    /**
     * Update the file list
     */
    private void updateFileList() {
        ProgramSettings settings = ProgramSettings.getInstance();
        settings.setVideosPath(textFieldSourceDir.getText());
        settings.saveSettings();
        
        // Read the file list
        VideoFilesReaderDialog reader = new VideoFilesReaderDialog(this, getCurrentVideoMap(), null);
        reader.showAndExecuteTask();
        List<VideoInfo> videoFiles = reader.getParsedVideos();
        reader.dispose();
        
        // Update the grid
        gridFiles.setRowCount(videoFiles.size());
        for (int i = 0; i < videoFiles.size(); i++) {
            VideoInfo video = videoFiles.get(i);
            
            gridFiles.setCellValue(Constants.COL_NAME_SELECTED, i, Boolean.FALSE);
            gridFiles.setCellValue(Constants.COL_NAME_FILE, i, video.getAbbreviatedName(textFieldSourceDir.getText()));
            gridFiles.setCellValue(Constants.COL_NAME_SIZE, i, video.getFormattedVideoFileSize());
            gridFiles.setCellValue(Constants.COL_NAME_DURATION, i, video.getFormattedVideoDuration());
            gridFiles.setCellValue(Constants.COL_NAME_VIDEO, i, video.getFormattedVideoCodec());
            gridFiles.setCellValue(Constants.COL_NAME_AUDIO, i, video);
            
        }
        updateCounters();
    }
    
    /**
     * Update the selected file counter
     */
    private void updateCounters() {
        long totalSelected = 0;
        long totalBytes = 0;
        int totalSeconds = 0;
        
        for (VideoInfo info : getSelectedVideos()) {
            totalSelected++;
            totalBytes += info.getBytes();
            totalSeconds += info.getNumberOfSeconds();
        }
        
        labelStatus.setText(String.format("Selected files: %d / %d (%s) - %s", totalSelected, gridFiles.getRowCount(),
                Utils.getFormattedByteCount(totalBytes), Utils.getTimeFromSeconds(totalSeconds)));
        
        // Enable the buttons
        buttonConvert.setEnabled(totalSelected > 0);
        buttonResize.setEnabled(totalSelected > 0);
        buttonRotate.setEnabled(totalSelected > 0);
        buttonInterval.setEnabled(totalSelected == 1);
        buttonCrop.setEnabled(totalSelected == 1);
        buttonConcatenate.setEnabled(totalSelected > 1);
    }
    
    /**
     * Check if a video is selected
     * 
     * @param row The grid row
     * @return
     */
    private boolean isVideoSelected(int row) {
        if (row > -1 && row < gridFiles.getRowCount()) {
            Object obj = gridFiles.getCellValue(Constants.COL_NAME_SELECTED, row);
            return BooleanUtils.isTrue((Boolean) obj);
        }
        return Boolean.FALSE;
    }
    
    /**
     * Set the video selection status on the grid
     * 
     * @param row The grid row
     * @param selected The new selecion state
     */
    private void setVideoSelection(int row, Boolean selected) {
        if (row > -1 && row < gridFiles.getRowCount()) {
            gridFiles.setCellValue(Constants.COL_NAME_SELECTED, row, selected);
        }
    }
    
    /**
     * Select / unSelect all videos
     * 
     * @param check The check state
     */
    private void setSelectionForAll(Boolean check) {
        for (int i = 0; i < gridFiles.getRowCount(); i++) {
            setVideoSelection(i, check);
        }
        updateCounters();
    }
    
    /**
     * Get the VideoInfo from a given grid rown
     * 
     * @param row The grid row
     * @return The VideoInfo
     */
    private VideoInfo getVideoInfo(int row) {
        if (row > -1 && row < gridFiles.getRowCount()) {
            return (VideoInfo) gridFiles.getCellValue(Constants.COL_NAME_AUDIO, row);
        }
        return null;
    }
    
    /**
     * Get a list with the selected videos
     * 
     * @return The selected video info
     */
    private List<VideoInfo> getSelectedVideos() {
        List<VideoInfo> selection = new ArrayList<>();
        for (int i = 0; i < gridFiles.getRowCount(); i++) {
            if (isVideoSelected(i)) {
                selection.add(getVideoInfo(i));
            }
        }
        return selection;
    }
    
    /**
     * Get a map of the current videos, to use as a cache
     * 
     * @return The map of videos
     */
    private Map<Path, VideoInfo> getCurrentVideoMap() {
        Map<Path, VideoInfo> resp = new HashMap<>();
        for (int i = 0; i < gridFiles.getRowCount(); i++) {
            VideoInfo info = getVideoInfo(i);
            if (info != null) {
                resp.put(info.getPath(), info);
            }
        }
        return resp;
    }
}
