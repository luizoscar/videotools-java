#!/usr/bin/env bash

# There is some kind of bug on Ubuntu when executing a parameter with spaces (like directory or file name with spaces)
# To circunvent this, we call this script, remove the quotes and call ffmpeg, this seems to work
ffmpeg "${@}"
exit 1